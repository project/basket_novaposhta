<?php

namespace Drupal\novaposhta\Commands;

use Drush\Commands\DrushCommands;

/**
 * Class of NovaPoshtaCommands.
 */
class NovaPoshtaCommands extends DrushCommands {

  /**
   * NovaPoshta update status (novaposhta:status_update)
   *
   * @command novaposhta:status_update
   * @usage novaposhta:status_update
   */
  public function novaposhtaStatusUpdate() {
    \Drupal::getContainer()->get('NovaPoshta')->cronRun();
  }

  /**
   * NovaPoshta update status (novaposhta:status_update)
   *
   * @command novaposhta:list
   * @usage novaposhta:list
   */
  public function novaposhtaUpdate($type) {
    \Drupal::getContainer()->get('NovaPoshta')->runUpdate($type);
  }

}
