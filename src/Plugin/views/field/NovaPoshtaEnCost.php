<?php

namespace Drupal\novaposhta\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Class of NovaPoshtaEnCost.
 *
 * @ViewsField("novaposhta_en_cost")
 */
class NovaPoshtaEnCost extends FieldPluginBase {

  /**
   * Renders a result row into a structured output.
   */
  public function render(ResultRow $values) {
    return [
      '#type' => 'inline_template',
      '#template' => '<span class="nowrap">{{ value|round(2) }} {{ currency }}</span>',
      '#context' => [
        'value' => $this->getValue($values),
        'currency' => \Drupal::getContainer()->get('NovaPoshta')->trans('UAH'),
      ],
    ];
  }

}
