<?php

namespace Drupal\novaposhta\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Class of NovaPoshtaEnWeight.
 *
 * @ViewsField("novaposhta_en_weight")
 */
class NovaPoshtaEnWeight extends FieldPluginBase {

  /**
   * Renders a template with a contextualized value and suffix.
   */
  public function render(ResultRow $values) {
    return [
      '#type' => 'inline_template',
      '#template' => '<span class="nowrap">{{ value|round(2) }} {{ suffix }}</span>',
      '#context' => [
        'value' => $this->getValue($values),
        'suffix' => \Drupal::getContainer()->get('NovaPoshta')->trans('kg'),
      ],
    ];
  }

}
