<?php

namespace Drupal\novaposhta\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Class of NovaPoshtaEnAddress.
 *
 * @ViewsField("novaposhta_en_address")
 */
class NovaPoshtaEnAddress extends FieldPluginBase {

  /**
   * Renders a template with the specified value from the result row.
   */
  public function render(ResultRow $values) {
    return [
      '#type' => 'inline_template',
      '#template' => '{{ value|raw }}',
      '#context' => [
        'value' => $this->getValue($values),
      ],
    ];
  }

}
