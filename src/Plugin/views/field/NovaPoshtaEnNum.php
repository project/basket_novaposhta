<?php

namespace Drupal\novaposhta\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Class of NovaPoshtaEnNum.
 *
 * @ViewsField("novaposhta_en_num")
 */
class NovaPoshtaEnNum extends FieldPluginBase {

  /**
   * Called to add the field to a query.
   */
  public function query() {
    parent::query();
    $this->query->addField('novaposhta_en', 'new_en_num', 'new_en_num');
  }

  /**
   * Renders the provided values into a formatted string.
   */
  public function render(ResultRow $values) {
    return implode(' -> ', array_filter([
      $this->getValue($values),
      $values->new_en_num ?? NULL,
    ]));
  }

}
