<?php

namespace Drupal\novaposhta\Plugin\Basket\Delivery;

use Drupal\basket\Plugins\Delivery\BasketDeliveryInterface;
use Drupal\novaposhta\API\NovaPoshtaAPI;
use Drupal\novaposhta\AdminPages;
use Drupal\novaposhta\NovaPoshtaEN;

/**
 * Class of NovaPoshtaDelivery.
 *
 * @BasketDelivery(
 *   id = "novaposhta",
 *   name = "NovaPoshta Delivery",
 * )
 */
class NovaPoshtaDelivery implements BasketDeliveryInterface {

  const NOVAPOSHTA_FIELDS = 'novaposhta_fields';

  /**
   * NovaPoshtaAPI object.
   *
   * @var \Drupal\novaposhta\API\NovaPoshtaAPI
   */
  protected $novaPoshtaAPI;

  /**
   * NovaPoshta object.
   *
   * @var \Drupal\novaposhta\NovaPoshta|object|null
   */
  protected $novaPoshta;

  /**
   * Is hide label.
   *
   * @var bool
   */
  protected $hideLabels;

  /**
   * Delivery sum info.
   *
   * @var mixed
   */
  protected static $deliverySum;

  /**
   * Style ID.
   *
   * @var array|mixed|string|null
   */
  protected $jQueryStyleID;

  /**
   * Basket object.
   *
   * @var \Drupal\basket\Basket|object|null
   */
  protected $basket;

  /**
   * Constructs a new instance.
   */
  public function __construct() {
    $this->novaPoshtaAPI = new NovaPoshtaAPI(\Drupal::config('novaposhta.settings')->get('config.api_key'));
    $this->novaPoshta = \Drupal::getContainer()->get('NovaPoshta');
    $this->hideLabels = !empty(\Drupal::config('novaposhta.settings')->get('config.hide_labels'));
    $this->jQueryStyleID = $this->novaPoshta->getJqueryStyleID();
    $this->basket = \Drupal::getContainer()->get('Basket');
  }

  /**
   * Retrieves the parent fields related to the basket.
   */
  public function basketFieldParents() {
    return [self::NOVAPOSHTA_FIELDS];
  }

  /**
   * Alters the basket form by adding region, city, and point selection fields.
   */
  public function basketFormAlter(&$form, $form_state) {
    $this->novaPoshta->jQuerySelectAttached($form);
    $values = $this->basketLoad($form_state);
    $form['region'] = [
      '#type' => 'select',
      '#title' => $this->novaPoshta->trans('Region'),
      '#title_display' => $this->hideLabels ? 'none' : 'before',
      '#empty_option' => $this->hideLabels ? $this->novaPoshta->trans('Region') : $this->novaPoshta->trans('Not specified'),
      '#options' => $this->novaPoshtaAPI->getRegions(),
      '#ajax' => !empty($form['#ajax_wrap']) ? $form['#ajax_wrap'] : [],
      '#required' => TRUE,
      '#attributes' => [
        'class' => [$this->jQueryStyleID . '_np_select'],
      ],
    ];
    if (!empty($values['region'])) {
      if (!empty($form['region']['#options'][$values['region']])) {
        $form['region']['#default_value'] = $values['region'];
      }
      else {
        unset($values['region']);
      }
    }

    $form['city'] = [
      '#type' => 'select',
      '#title' => $this->novaPoshta->trans('City'),
      '#title_display' => $this->hideLabels ? 'none' : 'before',
      '#empty_option' => $this->hideLabels ? $this->novaPoshta->trans('City') : $this->novaPoshta->trans('Not specified'),
      '#options' => $this->novaPoshtaAPI->getCitis([
        'region' => !empty($values['region']) ? $values['region'] : '',
      ]),
      '#ajax' => !empty($form['#ajax_wrap']) ? $form['#ajax_wrap'] : [],
      '#required' => TRUE,
      '#attributes' => [
        'class' => [$this->jQueryStyleID . '_np_select'],
      ],
    ];
    if (!empty($values['city'])) {
      if (!empty($form['city']['#options'][$values['city']])) {
        $form['city']['#default_value'] = $values['city'];
      }
      else {
        unset($values['city']);
      }
    }

    $form['point'] = [
      '#type' => 'select',
      '#title' => $this->novaPoshta->trans('Point'),
      '#title_display' => $this->hideLabels ? 'none' : 'before',
      '#empty_option' => $this->hideLabels ? $this->novaPoshta->trans('Point') : $this->novaPoshta->trans('Not specified'),
      '#options' => $this->novaPoshtaAPI->getPoints([
        'city' => !empty($values['city']) ? $values['city'] : '',
      ]),
      '#required' => TRUE,
      '#attributes' => [
        'class' => [$this->jQueryStyleID . '_np_select'],
      ],
    ];
    if (!empty($values['point'])) {
      if (!empty($form['point']['#options'][$values['point']])) {
        $form['point']['#default_value'] = $values['point'];
      }
      else {
        unset($values['point']);
      }
    }

    $this->enNum($form, $form_state);
  }

  /**
   * Modifies the form by adding an EN number field and performs alterations.
   */
  public function enNum(&$form, $form_state) {
    $entity = $form_state->getBuildInfo()['callback_object']->getEntity();
    if (!empty($entity->id()) && \Drupal::currentUser()->hasPermission('access novaposhta en') && $entity->getEntityTypeId() == 'node') {
      $order = $this->basket->Orders(NULL, $entity->id())->load();
      $np = NULL;
      if (!empty($order)) {
        $np = (new AdminPages)->loadOrderEN($order->id);
      }
      $form['enNum'] = [
        '#type' => 'textfield',
        '#title' => $this->novaPoshta->trans('EN'),
        '#default_value' => !empty($np->en_num) ? $np->en_num : NULL,
      ];
    }

    // Alter:
    \Drupal::moduleHandler()->alter('novaposhta_fields', $form, $form_state);

    $_SESSION['novaposhta']['recipientCity'] = !empty($values['city']) ? $values['city'] : NULL;
  }

  /**
   * Deletes basket data associated with the specified entity.
   */
  public function basketDelete($entity, $entity_delete) {
    \Drupal::database()->delete('novaposhta')
      ->condition('entity', $entity->getEntityTypeId())
      ->condition('entity_id', $entity->id())
      ->execute();
  }

  /**
   * Saves basket data and updates relevant information for the given entity.
   */
  public function basketSave($entity, $form_state) {
    $values = $form_state->getValue(self::NOVAPOSHTA_FIELDS);
    if (!empty($values)) {
      $address = [];
      if (!empty($values['region'])) {
        $regions = $this->novaPoshtaAPI->getRegions();
        if (!empty($regions[$values['region']])) {
          $address[] = $regions[$values['region']] . ' обл.';
        }
        if (!empty($values['city'])) {
          $citis = $this->novaPoshtaAPI->getCitis([
            'region' => $values['region'],
          ]);
          if (!empty($citis[$values['city']])) {
            $address[] = $citis[$values['city']];
          }
          if (!empty($values['point'])) {
            $points = $this->novaPoshtaAPI->getPoints([
              'city' => $values['city'],
            ]);
            if (!empty($points[$values['point']])) {
              $address[] = $points[$values['point']];
            }
          }
        }
      }
      \Drupal::database()->merge('novaposhta')
        ->keys([
          'entity' => $entity->getEntityTypeId(),
          'entity_id' => $entity->id(),
        ])
        ->fields([
          'entity' => $entity->getEntityTypeId(),
          'entity_id' => $entity->id(),
          'address' => implode(', ', $address),
          'data' => serialize($values),
        ])
        ->execute();

      // Save EN num:
      $order = $this->basket->orders(NULL, $entity->id())->load();
      if (!empty($order)) {
        (new NovaPoshtaEN())->updateOrderEnNum($order->id, (!empty($values['enNum']) ? trim($values['enNum']) : NULL));
      }
    }
  }

  /**
   * Loads basket data associated with the form state.
   */
  public function basketLoad($form_state) {
    $values = $form_state->getValue(self::NOVAPOSHTA_FIELDS);
    if (empty($values)) {
      $entity = $form_state->getBuildInfo()['callback_object']->getEntity();
      if ($entity->id()) {
        $load_data = \Drupal::database()->select('novaposhta', 'n')
          ->fields('n', ['data'])
          ->condition('n.entity', $entity->getEntityTypeId())
          ->condition('n.entity_id', $entity->id())
          ->execute()->fetchField();
        if (!empty($load_data)) {
          $values = unserialize($load_data);
        }
      }
    }
    return $values;
  }

  /**
   * Retrieves the address associated.
   */
  public function basketGetAddress($entity) {
    return \Drupal::database()->select('novaposhta', 'n')
      ->fields('n', ['address'])
      ->condition('n.entity', $entity->getEntityTypeId())
      ->condition('n.entity_id', $entity->id())
      ->execute()->fetchField();
  }

  /**
   * Alters the delivery information including cost, currency, and description.
   */
  public function deliverySumAlter(&$info) {
    $senderCity = \Drupal::config('novaposhta.en.template')->get('config.sender.city');
    $senderWeight = \Drupal::config('novaposhta.en.template')->get('config.Weight');
    $recipientCity = !empty($_SESSION['novaposhta']['recipientCity']) ? $_SESSION['novaposhta']['recipientCity'] : '';
    if (!isset(self::$deliverySum[$senderCity][$recipientCity])) {
      $sum = $this->basket->Cart()->getTotalSum(TRUE, TRUE);
      self::$deliverySum[$senderCity][$recipientCity] = $this->novaPoshtaAPI->getCost($senderCity, $recipientCity, $senderWeight, $sum);
    }
    if (!empty(self::$deliverySum[$senderCity][$recipientCity]['data'][0]['Cost'])) {
      $info['sum'] = self::$deliverySum[$senderCity][$recipientCity]['data'][0]['Cost'];
      $info['currency'] = 'UAH';
      $info['description'] = $this->novaPoshta->trans('Estimated Shipping Cost');
    }
    $info['isPay'] = FALSE;
  }

}
