<?php

namespace Drupal\novaposhta\Plugin\Basket\Delivery;

use Drupal\basket\Plugins\Delivery\BasketDeliveryInterface;
use Drupal\novaposhta\AdminPages;
use Drupal\novaposhta\API\NovaPoshtaAPI;
use Drupal\novaposhta\NovaPoshtaEN;

/**
 * Class of NovaPoshtaDeliveryAddress.
 *
 * @BasketDelivery(
 *   id = "novaposhta_address",
 *   name = "NovaPoshta Delivery Address",
 * )
 */
class NovaPoshtaDeliveryAddress implements BasketDeliveryInterface {

  const NOVAPOSHTA_FIELDS = 'novaposhta_address_fields';

  /**
   * NovaPoshtaAPI object.
   *
   * @var \Drupal\novaposhta\API\NovaPoshtaAPI
   */
  protected $novaPoshtaAPI;

  /**
   * NovaPoshta object.
   *
   * @var \Drupal\novaposhta\NovaPoshta|object|null
   */
  protected $novaPoshta;

  /**
   * Is hide label.
   *
   * @var bool
   */
  protected static $hideLabels;

  /**
   * Delivery sum info.
   *
   * @var mixed
   */
  protected static $deliverySum;
  /**
   * Style ID.
   *
   * @var array|mixed|string|null
   */
  protected static $jQueryStyleID;

  /**
   * Constructs a new instance.
   */
  public function __construct() {
    $this->novaPoshtaAPI = new NovaPoshtaAPI(\Drupal::config('novaposhta.settings')->get('config.api_key'));
    $this->novaPoshta = \Drupal::getContainer()->get('NovaPoshta');
    self::$hideLabels = !empty(\Drupal::config('novaposhta.settings')->get('config.hide_labels'));
    self::$jQueryStyleID = $this->novaPoshta->getJqueryStyleID();
  }

  /**
   * Retrieves the parent fields related to the basket.
   */
  public function basketFieldParents() {
    return [self::NOVAPOSHTA_FIELDS];
  }

  /**
   * Alters the basket form by adding region, city, and point selection fields.
   */
  public function basketFormAlter(&$form, $form_state) {
    $this->novaPoshta->JquerySelectAttached($form);
    $values = self::basketLoad($form_state);

    $form['city'] = [
      '#type' => 'textfield',
      '#title' => $this->novaPoshta->trans('City'),
      '#title_display' => self::$hideLabels ? 'none' : 'before',
      '#ajax' => !empty($form['#ajax_wrap']) ? $form['#ajax_wrap'] + ['event' => 'autocompleteclose'] : [],
      '#required' => TRUE,
      '#autocomplete_route_name' => 'basket.pages',
      '#autocomplete_route_parameters' => [
        'page_type' => 'api-novaposhta_get_cities',
      ],
      '#default_value' => !empty($values['city']) ? $values['city'] : NULL,
      '#attributes' => [
        'placeholder' => self::$hideLabels ? $this->novaPoshta->trans('Street') : '',
        'autocomplete' => 'anyrandomstring',
      ],
    ];

    $triggerElement = $form_state->getTriggeringElement();
    if (!empty($triggerElement['#name']) && $triggerElement['#name'] == 'novaposhta_address_fields[city]') {
      $input = &$form_state->getUserInput();
      $input['novaposhta_address_fields']['street'] = '';
      $form_state->setValue(['novaposhta_address_fields', 'street'], '');
    }

    $form['street'] = [
      '#type' => 'textfield',
      '#title' => $this->novaPoshta->trans('Street'),
      '#title_display' => self::$hideLabels ? 'none' : 'before',
      '#attributes' => [
        'placeholder' => self::$hideLabels ? $this->novaPoshta->trans('Street') : '',
        'autocomplete' => 'anyrandomstring',
      ],
      '#required' => TRUE,
      '#default_value' => !empty($values['street']) ? $values['street'] : NULL,
      '#autocomplete_route_name' => 'basket.pages',
      '#autocomplete_route_parameters' => [
        'page_type' => 'api-novaposhta_get_address',
        'ref' => $this->novaPoshtaAPI->getCityRefByName(
          $form_state->getValue(['novaposhta_address_fields', 'city'])
        ),
      ],
    ];

    $form['house'] = [
      '#type' => 'textfield',
      '#title' => $this->novaPoshta->trans('House number'),
      '#title_display' => self::$hideLabels ? 'none' : 'before',
      '#attributes' => [
        'placeholder' => self::$hideLabels ? $this->novaPoshta->trans('House number') : '',
      ],
      '#required' => TRUE,
      '#default_value' => !empty($values['house']) ? $values['house'] : NULL,
    ];

    $form['apartment'] = [
      '#type' => 'textfield',
      '#title' => $this->novaPoshta->trans('Apartment number'),
      '#title_display' => self::$hideLabels ? 'none' : 'before',
      '#attributes' => [
        'placeholder' => self::$hideLabels ? $this->novaPoshta->trans('Apartment number') : '',
      ],
      '#default_value' => !empty($values['apartment']) ? $values['apartment'] : NULL,
    ];
    $form['comment'] = [
      '#type' => 'textfield',
      '#title' => $this->novaPoshta->trans('Comment to the address'),
      '#title_display' => self::$hideLabels ? 'none' : 'before',
      '#attributes' => [
        'placeholder' => self::$hideLabels ? $this->novaPoshta->trans('Comment to the address') : '',
      ],
      '#default_value' => !empty($values['comment']) ? $values['comment'] : NULL,
    ];
    $entity = $form_state->getBuildInfo()['callback_object']->getEntity();
    if (!empty($entity->id()) && \Drupal::currentUser()->hasPermission('access novaposhta en') && $entity->getEntityTypeId() == 'node') {
      $order = \Drupal::service('Basket')->Orders(NULL, $entity->id())->load();
      $np = NULL;
      if (!empty($order)) {
        $np = (new AdminPages())->loadOrderEN($order->id);
      }
      $form['enNum'] = [
        '#type' => 'textfield',
        '#title' => $this->novaPoshta->trans('EN'),
        '#default_value' => !empty($np->en_num) ? $np->en_num : NULL,
      ];
    }

    // Alter:
    \Drupal::moduleHandler()->alter('novaposhta_fields', $form, $form_state);

    $_SESSION['novaposhta']['recipientCity'] = !empty($values['city']) ? $values['city'] : NULL;
  }

  /**
   * Deletes basket data associated with the specified entity.
   */
  public function basketDelete($entity, $entity_delete) {
    \Drupal::database()->delete('novaposhta')
      ->condition('entity', $entity->getEntityTypeId())
      ->condition('entity_id', $entity->id())
      ->execute();
  }

  /**
   * Saves basket data and updates relevant information for the given entity.
   */
  public function basketSave($entity, $form_state) {
    $values = $form_state->getValue(self::NOVAPOSHTA_FIELDS);
    if (!empty($values)) {
      $address = [];
      if (!empty($values['city'])) {
        $address[] = $values['city'];
        if (!empty($values['street'])) {
          $address[] = $values['street'];
        }
        if (!empty($values['house'])) {
          $address[] = ' ' . $values['house'];
        }
        if (!empty($values['apartment'])) {
          $address[] = $this->novaPoshta->trans('apt.') . ' ' . $values['apartment'];
        }
      }
      \Drupal::database()->merge('novaposhta')
        ->keys([
          'entity' => $entity->getEntityTypeId(),
          'entity_id' => $entity->id(),
        ])
        ->fields([
          'entity' => $entity->getEntityTypeId(),
          'entity_id' => $entity->id(),
          'address' => implode(', ', $address),
          'data' => serialize($values),
        ])
        ->execute();

      // Save EN num:
      $order = \Drupal::getContainer()->get('Basket')->orders(NULL, $entity->id())->load();
      if (!empty($order)) {
        (new NovaPoshtaEN())->updateOrderEnNum($order->id, (!empty($values['enNum']) ? trim($values['enNum']) : NULL));
      }
    }
  }

  /**
   * Loads basket data associated with the form state.
   */
  public function basketLoad($form_state) {
    $values = $form_state->getValue(self::NOVAPOSHTA_FIELDS);
    if (empty($values)) {
      $entity = $form_state->getBuildInfo()['callback_object']->getEntity();
      if ($entity->id()) {
        $load_data = \Drupal::database()->select('novaposhta', 'n')
          ->fields('n', ['data'])
          ->condition('n.entity', $entity->getEntityTypeId())
          ->condition('n.entity_id', $entity->id())
          ->execute()->fetchField();
        if (!empty($load_data)) {
          $values = unserialize($load_data);
        }
      }
    }
    return $values;
  }

  /**
   * Retrieves the address associated.
   */
  public function basketGetAddress($entity) {
    return \Drupal::database()->select('novaposhta', 'n')
      ->fields('n', ['address'])
      ->condition('n.entity', $entity->getEntityTypeId())
      ->condition('n.entity_id', $entity->id())
      ->execute()->fetchField();
  }

  /**
   * Alters the delivery information including cost, currency, and description.
   */
  public function deliverySumAlter(&$info) {
    $senderCity = \Drupal::config('novaposhta.en.template')->get('config.sender.city');
    $senderWeight = \Drupal::config('novaposhta.en.template')->get('config.Weight');
    $recipientCity = !empty($_SESSION['novaposhta']['recipientCity']) ? $_SESSION['novaposhta']['recipientCity'] : '';
    if (!isset(self::$deliverySum[$senderCity][$recipientCity])) {
      $sum = \Drupal::getContainer()->get('Basket')->cart()->getTotalSum(TRUE, TRUE);
      self::$deliverySum[$senderCity][$recipientCity] = $this->novaPoshtaAPI->getCost($senderCity, $recipientCity, $senderWeight, $sum);
    }
    if (!empty(self::$deliverySum[$senderCity][$recipientCity]['data'][0]['Cost'])) {
      $info['sum'] = self::$deliverySum[$senderCity][$recipientCity]['data'][0]['Cost'];
      $info['currency'] = 'UAH';
      $info['description'] = $this->novaPoshta->trans('Estimated Shipping Cost');
    }
    $info['isPay'] = FALSE;
  }

}
