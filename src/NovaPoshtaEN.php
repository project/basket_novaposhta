<?php

namespace Drupal\novaposhta;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\novaposhta\API\NovaPoshtaAPI;

/**
 * Class of NovaPoshtaEN.
 */
class NovaPoshtaEN {

  /**
   * NovaPoshta object.
   *
   * @var \Drupal\novaposhta\NovaPoshta|object|null
   */
  protected $novaPoshta;

  /**
   * NovaPoshtaAPI object.
   *
   * @var \Drupal\novaposhta\API\NovaPoshtaAPI
   */
  protected $api;

  /**
   * Class constructor.
   */
  public function __construct($apiKey = NULL) {
    $this->novaPoshta = \Drupal::getContainer()->get('NovaPoshta');
    $this->api = new NovaPoshtaAPI($apiKey);
  }

  /**
   * Processes and sets default values for the form based.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state of the form containing all submitted values.
   * @param object|null $editEN
   *   An optional object containing editable entity information.
   * @param int|null $orderId
   *   An optional order identifier for obtaining.
   */
  public function defValues(FormStateInterface $form_state, object $editEN = NULL, int $orderId = NULL) {
    $newValues = &$form_state->getValues();
    if (empty($newValues)) {

      // Edit info:
      if (!empty($editEN->all_info)) {
        $data = unserialize($editEN->all_info);
        $newValues['ServiceType'] = $data['ServiceType'];
        foreach ($data as $key => $value) {
          if (empty($value)) {
            continue;
          }
          switch ($key) {
            case'Weight':
            case'SeatsAmount':
            case'Cost':
            case'Description':
            case'PayerType':
            case'PaymentMethod':
            case'InfoRegClientBarcodes':
            case'CargoType':
              $newValues[$key] = $value;
              break;

            case'DateTime':
              $newValues['DateTime'] = date('Y-m-d', strtotime($value));
              break;

            case'Recipient':
            case'ContactRecipient':
            case'RecipientsPhone':
            case'RecipientsPhoneCustom':
              $newValues['recipient'][$key] = $value;
              break;

            case'Sender':
            case'ContactSender':
            case'SendersPhone':
              $newValues['sender'][$key] = $value;
              break;

            case'CitySender':
              if (!empty($value)) {
                $getCityInfo = $this->api->getCity([
                  'Ref' => $value,
                ]);
                if (!empty($getCityInfo['Area'])) {
                  $newValues['sender']['region'] = $getCityInfo['Area'];
                }
              }
              $newValues['sender']['city'] = $value;
              break;

            case'SenderAddress':
              $newValues['sender']['point'] = $value;
              break;

            case'CityRecipient':
              if (!empty($value)) {
                $getCityInfo = $this->api->getCity([
                  'Ref' => $value,
                ]);
                if (!empty($getCityInfo['Area'])) {
                  $newValues['recipient']['region'] = $getCityInfo['Area'];
                }
              }
              $newValues['recipient']['city'] = $value;
              break;

            case'RecipientAddress':
              $newValues['recipient']['point'] = $value;
              break;

            case'BackwardDeliveryMoney':
              if (!empty($value)) {
                $newValues['BackwardDelivery'] = [
                  'on' => TRUE,
                  'disabled' => TRUE,
                  'amount' => $value,
                ];
              }
              break;

            case'BackwardDeliveryData':
              if (!empty($value[0]['RedeliveryString']) && $value[0]['CargoType'] == 'Money') {
                $newValues['BackwardDelivery'] = [
                  'on' => TRUE,
                  'disabled' => TRUE,
                  'amount' => $value[0]['RedeliveryString'],
                ];
              }
              break;
          }
        }
      }
      else {
        $getValues = \Drupal::config('novaposhta.en.template')->get('config');
        if (!empty($getValues)) {
          $newValues = array_merge_recursive($newValues, $getValues);
        }
        if (!empty($newValues['sender']['Sender'])) {
          $senders = $this->api->getCounterparties([
            'CounterpartyProperty' => 'Sender',
          ]);
          if (empty($senders)) {
            $newValues = [];
          }
        }

        $newValues['DateTime'] = date('Y-m-d');
        $newValues['ServiceType'] = 'WarehouseWarehouse';

        // Order:
        $autoCreateRecipient = TRUE;
        if (!empty($orderId)) {
          $newValues['InfoRegClientBarcodes'] = $orderId;
          $order = \Drupal::getContainer()->get('Basket')->Orders($orderId)->load();
          if (!empty($order->nid)) {
            $orderData = \Drupal::database()->select('novaposhta', 'n')
              ->fields('n', ['data'])
              ->condition('n.entity', 'node')
              ->condition('n.entity_id', $order->nid)
              ->execute()->fetchField();
            if (!empty($orderData)) {
              $orderData = unserialize($orderData);
              if (!empty($orderData['region'])) {
                $newValues['recipient']['region'] = $orderData['region'];
              }
              if (!empty($orderData['city'])) {
                $newValues['recipient']['city'] = $orderData['city'];
              }
              if (!empty($orderData['point'])) {
                $newValues['recipient']['point'] = $orderData['point'];
              }
              if (!empty($orderData['street'])) {
                $newValues['recipient']['street'] = $orderData['street'];
              }
              if (!empty($orderData['house'])) {
                $newValues['recipient']['house'] = $orderData['house'];
              }
              if (!empty($orderData['apartment'])) {
                $newValues['recipient']['apartment'] = $orderData['apartment'];
              }
              if (!empty($orderData['comment'])) {
                $newValues['recipient']['Description'] = $orderData['comment'];
              }
            }
            $newValues['Cost'] = ceil($order->price);
            if (!empty($order->delivery_id)) {
              $deliveryService = \Drupal::service('Basket')->getSettings('delivery_services', $order->delivery_id);
              if (!empty($deliveryService) && $deliveryService == 'novaposhta_address') {
                $newValues['ServiceType'] = 'WarehouseDoors';
                $newValues['recipient']['RecipientType'] = 'PrivatePerson';
                $autoCreateRecipient = FALSE;
                $orderNode = \Drupal::entityTypeManager()->getStorage('node')->load($order->nid);
                $recipient = \Drupal::config('novaposhta.en.settings')->get('config.recipient');
                $recipientFields = $recipient['fields'];
                foreach ($recipientFields as &$value) {
                  $value = Html::decodeEntities(\Drupal::token()->replace($value, ['node' => $orderNode], ['clear' => TRUE]));
                }
                $newValues['recipient']['RecipientLastName'] = !empty($recipientFields['LastName']) ? $recipientFields['LastName'] : '';
                $newValues['recipient']['RecipientFirstName'] = !empty($recipientFields['FirstName']) ? $recipientFields['FirstName'] : '';
                $newValues['recipient']['RecipientMiddleName'] = !empty($recipientFields['MiddleName']) ? $recipientFields['MiddleName'] : '';
                $newValues['recipient']['RecipientsPhone'] = trim($recipientFields['Phone']);
              }
            }
          }
        }

        // Automatically create a recipient from an order:
        if (!empty($order->nid) && $autoCreateRecipient) {
          $orderNode = \Drupal::service('entity_type.manager')->getStorage('node')->load($order->nid);
          $this->autoCreateRecipient($form_state, $orderNode);
        }

        // Alter:
        \Drupal::moduleHandler()->alter('novaposhta_en_default', $newValues, $orderId);
      }
      $userInput = &$form_state->getUserInput();
      $userInput = $newValues;
    }
  }

  /**
   * Automatically creates a recipient based.
   *
   * This method processes and dynamically populates recipient fields using
   * configuration settings, replaces tokens within field values, and
   * invokes the API to save the recipient data. If successful, it updates
   * the form state with the corresponding recipient details.
   * Errors encountered during the process are logged as system messages.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state of the form containing all submitted values.
   * @param object $orderNode
   *   The order node entity used for token replacement.
   */
  public function autoCreateRecipient(FormStateInterface $form_state, $orderNode) {
    $recipient = \Drupal::config('novaposhta.en.settings')->get('config.recipient');
    if (!empty($recipient['auto'])) {
      $newValues = &$form_state->getValues();
      $recipientFields = $recipient['fields'];
      foreach ($recipientFields as $kv => $value) {
        $recipientFields[$kv] = Html::decodeEntities(\Drupal::token()->replace($value, ['node' => $orderNode], ['clear' => TRUE]));
        if (str_contains($value, '{{')) {
          $value = [
            '#type' => 'inline_template',
            '#template' => $value,
            '#context' => [
              'node' => $orderNode,
            ],
          ];
          $recipientFields[$kv] = \Drupal::service('renderer')->renderRoot($value);
          if (!empty($recipientFields[$kv])) {
            $recipientFields[$kv] = $recipientFields[$kv]->__toString();
          }
        }
      }
      if (!empty($recipientFields['Phone']) && !empty($recipientFields['LastName']) && !empty($recipientFields['FirstName'])) {

        $recipientFields['Phone'] = $this->novaPoshta->replacePhone(trim($recipientFields['Phone']));
        $recipientFields['CounterpartyType'] = 'PrivatePerson';
        $recipientFields['CounterpartyProperty'] = 'Recipient';

        $result = $this->api->saveCounterparty($recipientFields);
        if (!empty($result['data'][0]['Ref'])) {
          $newValues['recipient']['Recipient'] = $result['data'][0]['Ref'];
          if (!empty($result['data'][0]['ContactPerson']['data'][0]['Ref'])) {
            $newValues['recipient']['ContactRecipient'] = $result['data'][0]['ContactPerson']['data'][0]['Ref'];
            $newValues['recipient']['RecipientsPhone'] = $recipientFields['Phone'];
            $newValues['recipient']['LastName'] = $result['data'][0]['LastName'];
            $newValues['recipient']['FirstName'] = $result['data'][0]['FirstName'];
          }
        }
        elseif (!empty($result['errors'])) {
          foreach ($result['errors'] as $error) {
            \Drupal::messenger()->addMessage($error, 'error');
          }
        }
      }
    }
  }

  /**
   * Handles the creation or updating of an electronic document based.
   *
   * This method collects data from the form state to configure the parameters
   * needed for creating or updating an electronic document (EN). It processes
   * sender and recipient information, adjusts phone numbers if necessary, and
   * handles specifics such as address formatting, service types, delivery
   * options, and additional costs. Depending on the triggering element,
   * it either creates a new document or updates an existing one, while
   * performing validation and handling errors as required.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state of the form containing all submitted values.
   * @param object|null $editEN
   *   The existing electronic document object to be edited.
   */
  public function setEn(FormStateInterface $form_state, $editEN = NULL) {
    $values = $form_state->getValues();
    $triggerElement = $form_state->getTriggeringElement();

    if ($values['sender']['SendersPhone'] == 'PhoneCustom' && !empty($values['sender']['SendersPhoneCustom'])) {
      $values['sender']['SendersPhone'] = $values['sender']['SendersPhoneCustom'];
    }
    if ($values['recipient']['RecipientsPhone'] == 'PhoneCustom' && !empty($values['recipient']['RecipientsPhoneCustom'])) {
      $values['recipient']['RecipientsPhone'] = $values['recipient']['RecipientsPhoneCustom'];
    }
    $params = [
      'NewAddress' => 1,
      'PayerType' => $values['PayerType'],
      'PaymentMethod' => $values['PaymentMethod'],
      'CargoType' => $values['CargoType'],
      'Weight' => $values['Weight'],
      'ServiceType' => $values['ServiceType'],
      'SeatsAmount' => $values['SeatsAmount'],
      'Description' => trim($values['Description']),
      'Cost' => $values['Cost'],
      'CitySender' => $values['sender']['city'],
      'Sender' => $values['sender']['Sender'],
      'SenderAddress' => $values['sender']['point'],
      'ContactSender' => $values['sender']['ContactSender'],
      'SendersPhone' => str_replace(['-', ')', '('], '', $values['sender']['SendersPhone']),
      'RecipientsPhone' => str_replace(['-', ')', '('], '', $values['recipient']['RecipientsPhone']),
      'InfoRegClientBarcodes' => !empty($values['InfoRegClientBarcodes']) ? trim($values['InfoRegClientBarcodes']) : '',
      'DateTime' => date('d.m.Y', strtotime($values['DateTime'])),
    ];

    if (!empty($values['OptionsSeat'])) {
      $params['OptionsSeat'][0] = [
        'weight' => $params['Weight'],
      ];
      [
        $params['OptionsSeat'][0]['volumetricHeight'],
        $params['OptionsSeat'][0]['volumetricWidth'],
        $params['OptionsSeat'][0]['volumetricLength'],
      ] = explode('x', $values['OptionsSeat']);
      $params['OptionsSeat'][0]['volumetricVolume'] = round(($params['OptionsSeat'][0]['volumetricWidth'] / 100) * ($params['OptionsSeat'][0]['volumetricLength'] / 100) * ($params['OptionsSeat'][0]['volumetricHeight'] / 100), 2);
    }

    switch ($values['ServiceType']) {
      case'WarehouseDoors':
        $city = $this->api->searchSettlements($values['recipient']['city']);
        if (!empty($city['data'][0]['Addresses'][0])) {
          $params['RecipientArea'] = $city['data'][0]['Addresses'][0]['Area'];
          $params['RecipientCityName'] = $city['data'][0]['Addresses'][0]['MainDescription'];
        }
        $params['RecipientAreaRegions'] = '';
        $params['RecipientAddressName'] = trim($values['recipient']['street']);
        $params['RecipientHouse'] = trim($values['recipient']['house']);
        $params['RecipientFlat'] = '';
        $params['RecipientName'] = implode(' ', [
          $values['recipient']['RecipientLastName'],
          $values['recipient']['RecipientFirstName'],
          $values['recipient']['RecipientMiddleName'],
        ]);
        $params['RecipientType'] = $values['recipient']['RecipientType'];
        $params['Description'] = t('Site order');
        $params['NewAddress'] = 1;
        break;

      default:
        $params['CityRecipient'] = $values['recipient']['city'];
        $params['RecipientAddress'] = $values['recipient']['point'];
        $params['ContactRecipient'] = $values['recipient']['ContactRecipient'];
        $params['Recipient'] = $values['recipient']['Recipient'];
        break;
    }

    if (!empty($values['BackwardDelivery']['on']) && !empty($values['BackwardDelivery']['amount'])) {
      $params['BackwardDeliveryData'] = [
        [
          'PayerType' => 'Recipient',
          'CargoType' => 'Money',
          'RedeliveryString' => $values['BackwardDelivery']['amount'],
        ],
      ];
    }
    if (!empty($values['AfterpaymentOnGoodsCost']['on']) && !empty($values['AfterpaymentOnGoodsCost']['amount'])) {
      $params['AfterpaymentOnGoodsCost'] = $values['AfterpaymentOnGoodsCost']['amount'];
    }
    switch ($triggerElement['#name']) {
      case'insert':
        $insert = $this->api->internetDocument($params);
        if (!empty($insert['data'][0]['IntDocNumber'])) {
          $params += $insert['data'][0];

          // Address:
          if (!empty($params['CityRecipient'])) {
            $getCityInfo = $this->api->getCity([
              'Ref' => $params['CityRecipient'],
            ]);
            if (!empty($getCityInfo['Description'])) {
              if (!empty($getCityInfo['SettlementTypeDescription'])) {
                $params['CityRecipientDescription'] = $getCityInfo['SettlementTypeDescription'] . ' ' . $getCityInfo['Description'];
              }
            }
            if (!empty($params['RecipientAddress'])) {
              $points = $this->api->getPoints([
                'city' => $params['CityRecipient'],
              ]);
              if (!empty($points) && !empty($points[$params['RecipientAddress']])) {
                $params['RecipientAddressDescription'] = $points[$params['RecipientAddress']];
              }
            }
          }

          $this->api->insertEn($params);
          $form_state->set('IntDocNumber', $params['IntDocNumber']);

          if (!empty(trim($params['InfoRegClientBarcodes']))) {
            $this->updateOrderEnNum(trim($params['InfoRegClientBarcodes']), $params['IntDocNumber']);
          }
        }
        else {
          if (!empty($insert['errors'])) {
            foreach ($insert['errors'] as $error) {
              $form_state->setErrorByName($error, $error);
            }
          }
        }
        break;

      case'update':
        if (!empty($editEN->ref)) {
          $params['Ref'] = $editEN->ref;
        }
        $update = $this->api->internetDocumentUpdate($params);
        if (empty($update['data'][0]['IntDocNumber']) && !empty($update['errors'])) {
          foreach ($update['errors'] as $error) {
            $form_state->setErrorByName($error, $this->novaPoshta->trans(trim($error)));
          }
        }
        break;
    }
  }

  /**
   * Updates the EN (TTN) number for a given order and logs the changes.
   *
   * @param int|string $order_id
   *   The unique identifier of the order for which the EN number is updated.
   * @param string $en_num
   *   The new EN (TTN) number to be associated with the order.
   */
  public function updateOrderEnNum(int | string $order_id, string $en_num) {

    // Get Old EN:
    $oldEN = \Drupal::database()->select('novaposhta_en_orders', 'en')
      ->fields('en', ['en_num'])
      ->condition('en.order_id', $order_id)
      ->execute()->fetchField();

    // Update:
    \Drupal::database()->merge('novaposhta_en_orders')
      ->keys([
        'order_id' => $order_id,
      ])
      ->fields([
        'en_num' => $en_num,
      ])
      ->execute();

    // Set logs:
    if (\Drupal::hasService('BasketLogs')) {
      $logs = [];
      if (empty($oldEN)) {
        $logs[] = [
          'type' => $this->novaPoshta->trans('Added TTN'),
          'new' => $en_num,
        ];
      }
      elseif ($oldEN != $en_num) {
        $logs[] = [
          'type' => $this->novaPoshta->trans('Changed TTN'),
          'old' => $oldEN,
          'new' => $en_num,
        ];
      }
      \Drupal::getContainer()->get('BasketLogs')->trigger('custom', [
        'orderID' => $order_id,
        'logs' => $logs,
      ]);
    }
  }

}
