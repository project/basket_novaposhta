<?php

namespace Drupal\novaposhta\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class of NovaPoshtaSettingsForm.
 */
class NovaPoshtaSettingsForm extends FormBase {

  /**
   * NovaPoshta object.
   *
   * @var \Drupal\novaposhta\NovaPoshta|object|null
   */
  protected $novaPoshta;

  /**
   * Config settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Config EN settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $configEn;

  /**
   * ModuleHandler object.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructor for initializing the class.
   */
  public function __construct() {
    $this->novaPoshta = \Drupal::getContainer()->get('NovaPoshta');
    $this->config = \Drupal::config('novaposhta.settings');
    $this->configEn = \Drupal::config('novaposhta.en.settings');
    $this->moduleHandler = \Drupal::moduleHandler();
  }

  /**
   * Retrieves the form ID associated with the Nova Poshta settings form.
   */
  public function getFormId() {
    return 'novaposhta_settings_form';
  }

  /**
   * Builds the configuration form with various sections and options.
   *
   * @param array $form
   *   The form structure array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->novaPoshta->trans('API'),
      '#open' => FALSE,
      '#tree' => TRUE,
      'api_key' => [
        '#type' => 'textfield',
        '#title' => $this->novaPoshta->trans('API KEY'),
        '#default_value' => $this->config->get('config.api_key'),
      ],
      'hide_labels' => [
        '#type' => 'checkbox',
        '#title' => $this->novaPoshta->trans('Hide labels of the fields of the basket'),
        '#default_value' => $this->config->get('config.hide_labels'),
      ],
      'jquery_style' => [
        '#type' => 'select',
        '#title' => 'jQuery style',
        '#required' => TRUE,
        '#options' => $this->getJqueryStyleOptions(),
        '#default_value' => $this->config->get('config.jquery_style'),
      ],
      'actions' => [
        '#type' => 'actions',
        'submit' => [
          '#type' => 'submit',
          '#value' => $this->novaPoshta->trans('Save API KEY'),
          '#attributes' => [
            'class' => ['button--primary'],
          ],
          '#name' => 'saveAPI',
        ],
      ],
    ];
    $home = $_SERVER['HOME'] ?? '';
    if (empty($home) && !empty($_SERVER['DOCUMENT_ROOT'])) {
      $home = dirname($_SERVER['DOCUMENT_ROOT']);
    }
    $form['en'] = [
      '#type' => 'details',
      '#title' => $this->novaPoshta->trans('Invoices'),
      '#open' => FALSE,
      '#tree' => TRUE,
      'scheduler' => [
        '#title' => $this->novaPoshta->trans('Scheduler'),
        '#type' => 'textarea',
        '#value' => 'export HOME="' . $home . '"; /usr/bin/php ' . DRUPAL_ROOT . '/vendor/drush/drush/drush.php -r ' . DRUPAL_ROOT . ' novaposhta:status_update',
        '#parents' => ['scheduler'],
        '#disabled' => TRUE,
        '#rows' => 2,
        '#description' => $this->novaPoshta->trans('Automatically launch status updates through server settings'),
      ],
      'recipient' => [
        'auto' => [
          '#type' => 'checkbox',
          '#title' => $this->novaPoshta->trans('Automatically create a recipient from an order'),
          '#default_value' => $this->configEn->get('config.recipient.auto'),
        ],
        'fields' => [
          '#type' => 'container',
          '#states' => [
            'visible' => ['input[name="en[recipient][auto]"]' => ['checked' => TRUE]],
          ],
          'LastName' => [
            '#type' => 'textfield',
            '#title' => $this->novaPoshta->trans('Surname'),
            '#default_value' => $this->configEn->get('config.recipient.fields.LastName'),
            '#description' => '[token] / {{ node.*** }}',
          ],
          'FirstName' => [
            '#type' => 'textfield',
            '#title' => $this->novaPoshta->trans('Name'),
            '#default_value' => $this->configEn->get('config.recipient.fields.FirstName'),
            '#description' => '[token] / {{ node.*** }}',
          ],
          'MiddleName' => [
            '#type' => 'textfield',
            '#title' => $this->novaPoshta->trans('Middle name'),
            '#default_value' => $this->configEn->get('config.recipient.fields.MiddleName'),
            '#description' => '[token] / {{ node.*** }}',
          ],
          'Phone' => [
            '#type' => 'textfield',
            '#title' => $this->novaPoshta->trans('Phone'),
            '#default_value' => $this->configEn->get('config.recipient.fields.Phone'),
            '#description' => '[token] / {{ node.*** }}',
          ],
          'Email' => [
            '#type' => 'textfield',
            '#title' => $this->novaPoshta->trans('E-mail'),
            '#default_value' => $this->configEn->get('config.recipient.fields.Email'),
            '#description' => '[token] / {{ node.*** }}',
          ],
          'token' => [
            '#theme' => 'token_tree_link',
            '#token_types' => ['node'],
            '#text' => $this->novaPoshta->trans('[available tokens]'),
          ],
        ],
      ],
      'print' => [
        '#type' => 'select',
        '#title' => $this->novaPoshta->trans('Printing forms'),
        '#options' => [
          'printDocumentPdf' => $this->novaPoshta->trans('Print by Number - PDF'),
          'printMarking100x100' => $this->novaPoshta->trans('Print 100x100 - PDF'),
          'printMarking85x85' => $this->novaPoshta->trans('Print 85x85 - PDF'),
        ],
        '#default_value' => $this->configEn->get('config.print'),
      ],
      'actions' => [
        '#type' => 'actions',
        'submit' => [
          '#type' => 'submit',
          '#name' => 'saveEN',
          '#value' => $this->novaPoshta->trans('Save configuration'),
          '#attributes' => [
            'class' => ['button--primary'],
          ],
        ],
      ],
    ];
    $form['OptionsSeat'] = [
      '#type' => 'details',
      '#title' => $this->novaPoshta->trans('The size of the parcel for the post office'),
      '#open' => FALSE,
      'sizes' => [
        '#type' => 'table',
        '#header' => [
          $this->novaPoshta->trans('Height'),
          $this->novaPoshta->trans('Width'),
          $this->novaPoshta->trans('Length'),
        ],
      ],
      'actions' => [
        '#type' => 'actions',
        'submit' => [
          '#type' => 'submit',
          '#name' => 'saveOptionsSeat',
          '#value' => $this->novaPoshta->trans('Save configuration'),
          '#attributes' => [
            'class' => ['button--primary'],
          ],
        ],
      ],
    ];

    foreach ($this->novaPoshta->defOptionsSeat() as $line) {
      $form['OptionsSeat']['sizes'][] = [
        'height' => [
          '#markup' => $line[0],
        ],
        'width' => [
          '#markup' => $line[1],
        ],
        'length' => [
          '#markup' => $line[2],
        ],
      ];
    }
    $sConfig = \Drupal::config('novaposhta.OptionsSeat');
    foreach (range(0, 5) as $k) {
      $form['OptionsSeat']['sizes'][] = [
        'height' => [
          '#type' => 'number',
          '#parents' => ['OptionsSeat', 'sizes', $k, 'height'],
          '#default_value' => $sConfig->get('config.sizes.' . $k . '.height'),
        ],
        'width' => [
          '#type' => 'number',
          '#parents' => ['OptionsSeat', 'sizes', $k, 'width'],
          '#default_value' => $sConfig->get('config.sizes.' . $k . '.width'),
        ],
        'length' => [
          '#type' => 'number',
          '#parents' => ['OptionsSeat', 'sizes', $k, 'length'],
          '#default_value' => $sConfig->get('config.sizes.' . $k . '.length'),
        ],
      ];
    }
    return $form;
  }

  /**
   * Handles the form submission for the specified configuration options.
   *
   * Based on the triggering element, saves different configurations for the
   * Nova Poshta settings and clears the related temporary or cached data.
   *
   * @param array $form
   *   The form structure to be processed and submitted.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggerElement = $form_state->getTriggeringElement();
    if (!empty($triggerElement['#name'])) {
      switch ($triggerElement['#name']) {
        case'saveAPI':
          // Clear cache settings:
          $this->novaPoshta->clearTmp();

          // Clear templates:
          $configTemplate = \Drupal::config('novaposhta.en.template')->get('config');
          $configTemplate['sender'] = [];
          \Drupal::configFactory()->getEditable('novaposhta.en.template')
            ->set('config', $configTemplate)
            ->save();

          \Drupal::configFactory()->getEditable('novaposhta.settings')
            ->set('config', $form_state->getValue('api'))
            ->save();
          break;

        case'saveEN':
          \Drupal::configFactory()->getEditable('novaposhta.en.settings')
            ->set('config', $form_state->getValue('en'))
            ->save();
          break;

        case'saveOptionsSeat':
          \Drupal::configFactory()->getEditable('novaposhta.OptionsSeat')
            ->set('config', $form_state->getValue('OptionsSeat'))
            ->save();
          break;
      }
      \Drupal::messenger()->addMessage($this->novaPoshta->trans('The configuration options have been saved.'));
    }
  }

  /**
   * Retrieves a list of jQuery style options available for use.
   */
  private function getJqueryStyleOptions(): array {
    $options = [
      'chosen' => 'Chosen',
    ];
    if ($this->moduleHandler->moduleExists('webform')) {
      if (\Drupal::getContainer()->get('webform.libraries_manager')->getLibrary('jquery.select2')) {
        $options['select2'] = 'select2';
      }
    }
    return $options;
  }

}
