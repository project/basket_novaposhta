<?php

namespace Drupal\novaposhta\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\novaposhta\API\NovaPoshtaAPI;
use Drupal\Core\Url;

/**
 * Class of NovaPoshtaENForm.
 */
class NovaPoshtaENForm extends FormBase {

  /**
   * NovaPoshta object.
   *
   * @var \Drupal\novaposhta\NovaPoshta|object|null
   */
  protected $novaPoshta;

  /**
   * NovaPoshtaAPI object.
   *
   * @var \Drupal\novaposhta\API\NovaPoshtaAPI
   */
  protected $api;

  /**
   * Is template.
   *
   * @var mixed
   */
  protected $isTemplate;

  /**
   * EN ID.
   *
   * @var mixed
   */
  protected $editEN;

  /**
   * Order ID.
   *
   * @var mixed
   */
  protected $orderId;

  /**
   * Ajax array.
   *
   * @var array
   */
  protected $senderAjax;

  /**
   * Ajax array.
   *
   * @var array
   */
  protected $recipientAjax;

  /**
   * Ajax array.
   *
   * @var array
   */
  protected $cargoAjax;

  /**
   * Ajax array.
   *
   * @var array
   */
  protected $backwardDelivery;

  /**
   * Ajax array.
   *
   * @var array
   */
  protected $afterpaymentOnGoodsCost;

  /**
   * Style ID.
   *
   * @var array|mixed|string|null
   */
  protected $jQueryStyleID;

  /**
   * Basket object.
   *
   * @var \Drupal\basket\Basket|object|null
   */
  protected $basket;

  /**
   * Constructs a new instance of the class.
   *
   * @param mixed|null $isTemplate
   *   Specifies whether to use a template or not. Defaults to NULL.
   * @param mixed|null $editEN
   *   Indicates edit mode for the entity. Defaults to NULL.
   * @param int|null $orderId
   *   The order ID associated with the instance. Defaults to NULL.
   */
  public function __construct(mixed $isTemplate = NULL, mixed $editEN = NULL, int $orderId = NULL) {
    $this->novaPoshta = \Drupal::getContainer()->get('NovaPoshta');
    $this->api = new NovaPoshtaAPI();
    $this->isTemplate = $isTemplate;
    $this->editEN = $editEN;
    $this->orderId = $orderId;
    $this->jQueryStyleID = $this->novaPoshta->getJqueryStyleId();
    $this->basket = \Drupal::getContainer()->get('Basket');

    $this->senderAjax = [
      'wrapper' => 'novaposhta_en_sender_ajax_wrap',
      'callback' => [$this, 'ajaxSenderReload'],
    ];
    $this->recipientAjax = [
      'wrapper' => 'novaposhta_en_recipient_ajax_wrap',
      'callback' => [$this, 'ajaxRecipientReload'],
    ];
    $this->cargoAjax = [
      'wrapper' => 'novaposhta_en_Cargo_ajax_wrap',
      'callback' => [$this, 'ajaxCargoReload'],
    ];
    $this->backwardDelivery = [
      'wrapper' => 'novaposhta_en_BackwardDelivery_ajax_wrap',
      'callback' => [$this, 'ajaxBackwardDeliveryReload'],
    ];
    $this->afterpaymentOnGoodsCost = [
      'wrapper' => 'novaposhta_en_AfterpaymentOnGoodsCost_ajax_wrap',
      'callback' => [$this, 'ajaxAfterpaymentOnGoodsCostReload'],
    ];
  }

  /**
   * Retrieves the unique form ID for the current form.
   */
  public function getFormId() {
    return 'novaposhta_en_form';
  }

  /**
   * Builds the form structure.
   *
   * @param array $form
   *   The render array of the form to be built.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Set default value:
    if (empty($form_state->getValues())) {
      $this->novaPoshta->eN()->defValues($form_state, $this->editEN, $this->orderId);
    }
    if (!empty($this->editEN) && empty($this->editEN->ref)) {
      return $this->basket->getError(404);
    }

    $triggerElement = $form_state->getTriggeringElement();
    if (!empty($triggerElement['#name']) && $triggerElement['#name'] == 'ajaxSetValue') {
      $this->changeFormState($form_state);
    }

    $form += [
      '#prefix' => '<div id="novaposhta_en_form_ajax_wrap">',
      '#suffix' => '</div>',
      '#attached' => [
        'library' => ['novaposhta/admin'],
      ],
    ];
    $this->novaPoshta->jQuerySelectAttached($form);

    if (!empty($this->editEN) && $form_state->getValue('ServiceType') == 'WarehouseDoors') {
      return $this->basket->getError(403, $this->novaPoshta->trans('Editing is only available in the business office'));
    }

    // ServiceType:
    $form['ServiceType'] = [
      '#type' => 'select',
      '#title' => $this->novaPoshta->trans('Delivery type'),
      '#options' => [
        'WarehouseWarehouse' => $this->novaPoshta->trans('Warehouse - Warehouse'),
        'WarehouseDoors' => $this->novaPoshta->trans('Warehouse - Address'),
      ],
      '#required' => TRUE,
      '#weight' => -100,
      '#default_value' => $form_state->getValue('ServiceType'),
      '#ajax' => [
        'wrapper' => 'novaposhta_en_form_ajax_wrap',
        'callback' => [$this, 'ajaxReload'],
      ],
    ];
    if (empty($form_state->getValue('ServiceType'))) {
      return $form;
    }
    if (!empty($this->editEN)) {
      $form['ServiceType']['#disabled'] = TRUE;
    }
    $form['users'] = [
      '#type' => 'table',
      '#header' => [
        $this->novaPoshta->trans('Sender data'),
        $this->novaPoshta->trans('Recipient data'),
      ],
    ];
    $form['ajaxSetValue'] = [
      '#type' => 'textarea',
      '#ajax' => [
        'wrapper' => 'novaposhta_en_form_ajax_wrap',
        'callback' => [$this, 'ajaxReload'],
        'event' => 'change',
      ],
      '#attributes' => [
        'style' => 'display:none;',
      ],
    ];

    // Sender data:
    $form['users']['']['sender'] = [
      '#parents' => ['sender'],
      '#prefix' => '<div id="' . $this->senderAjax['wrapper'] . '">',
      '#suffix' => '</div>',
      '#wrapper_attributes' => [
        'class' => ['not_hover'],
        'width' => '50%',
      ],
    ];
    $options = [
      '_none' => $this->novaPoshta->trans('No template'),
    ];
    $templates = \Drupal::config('novaposhta.en.template')->get('config.sender.templates') ?? [];
    $form_state->setValue(['sender', 'templates'], $templates);
    $merge_templates = [];
    foreach ($templates as $key => $template) {
      $merge_templates[$key] = $key;
    }
    $options = array_replace_recursive($options, $merge_templates);
    $options['_new'] = $this->novaPoshta->trans('Create a new template');
    $form['users']['']['sender']['template'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->novaPoshta->trans('Sender template'),
      '#default_value' => $form_state->getValue(['sender', 'template']) ?? '_none',
      '#template_change' => TRUE,
      '#ajax' => $this->senderAjax,
    ];
    $triggerElement = $form_state->getTriggeringElement();
    $template = $form_state->getValue(['sender', 'template']);
    if (isset($templates[$template]) && !empty($triggerElement['#template_change'])) {
      $sender_data = $form_state->getValue('sender');
      $sender_data = array_replace_recursive($sender_data, $templates[$template]);
      $form_state->setValue('sender', $sender_data);

      $user_input = $form_state->getUserInput();
      $user_input['sender'] = array_replace_recursive($user_input['sender'], $templates[$template]);
      $form_state->setUserInput($user_input);
    }

    if ($template === '_new' && !empty($triggerElement['#template_change'])) {
      $user_input = $form_state->getUserInput();
      if (isset($user_input['sender']['template_name'])) {
        unset($user_input['sender']['template_name']);
        $form_state->setUserInput($user_input);
      }
    }
    if ($template && $template !== '_none' && $template !== '_new') {
      $form['users']['']['sender']['template_remove'] = [
        '#type' => 'button',
        '#ajax' => [
          'wrapper' => 'novaposhta_en_sender_ajax_wrap',
          'callback' => [$this, 'removeTemplate'],
        ],
        '#template_name' => $form_state->getValue(['sender', 'template_name']),
        '#value' => $this->novaPoshta->trans('Delete template'),
        '#attributes' => [
          'style' => 'margin: 0; background: red !important;',
        ],
      ];
    }
    if ($template && $template !== '_none') {
      $form['users']['']['sender']['template_name'] = [
        '#type' => 'textfield',
        '#title' => $this->novaPoshta->trans('Template name'),
        '#default_value' => $template === '_new'
        ? NULL
        : $form_state->getValue(['sender', 'template_name']),
        '#required' => TRUE,
      ];
    }

    $this->setAddress($form['users']['']['sender'], $form_state, $this->senderAjax);
    $form['users']['']['sender']['Sender'] = [
      '#type' => 'select',
      '#title' => $this->novaPoshta->trans('Sender'),
      '#options' => $this->api->getCounterparties([
        'CounterpartyProperty' => 'Sender',
      ]),
      '#required' => TRUE,
      '#ajax' => $this->senderAjax,
      '#default_value' => $form_state->getValue(['sender', 'Sender']),
    ];
    $senderRef = $form_state->getValue(['sender', 'Sender']);
    $form['users']['']['sender']['ContactSender'] = [
      '#type' => 'select',
      '#title' => $this->novaPoshta->trans('Contact person'),
      '#options' => $this->api->getCounterpartyContactPersons([
        'Ref' => $senderRef ?? NULL,
      ]),
      '#required' => TRUE,
      '#empty_option' => t('- Select -'),
      '#ajax' => $this->senderAjax,
      '#default_value' => $form_state
        ->getValue(['sender', 'ContactSender']),
    ];

    $sendersPhones = [];
    $contactSender = $form_state->getValue(['sender', 'ContactSender']);
    if (!empty($senderRef) && !empty($contactSender)) {
      $sendersPhones = $this->api->getCounterpartyContactPersons([
        'Ref' => $senderRef,
        'Contact' => $contactSender,
      ]);
    }
    $form['users']['']['sender']['SendersPhone'] = [
      '#type' => 'select',
      '#title' => $this->novaPoshta->trans('Sender\'s phone'),
      '#options' => $sendersPhones,
      '#required' => TRUE,
      '#empty_option' => t('- Select -'),
      '#ajax' => $this->senderAjax,
      '#default_value' => $form_state->getValue(['sender', 'SendersPhone']),
    ];
    $sendersPhone = $form_state->getValue(['sender', 'SendersPhone']);
    if (!empty($sendersPhone) && !empty($senderRef)) {
      if ($sendersPhone == 'PhoneCustom') {
        $form['users']['']['sender']['SendersPhoneCustom'] = [
          '#type' => 'textfield',
          '#title' => $this->novaPoshta->trans('Sender\'s phone'),
          '#required' => TRUE,
          '#attributes' => [
            'data-mask' => $this->novaPoshta::PHONE_MASK,
            'placeholder' => str_replace(9, '_', $this->novaPoshta::PHONE_MASK),
          ],
          '#default_value' => $form_state
            ->getValue(['sender', 'SendersPhoneCustom']),
        ];
      }
    }

    // Recipient data:
    $form['users']['']['recipient'] = [
      '#parents' => ['recipient'],
      '#prefix' => '<div id="' . $this->recipientAjax['wrapper'] . '">',
      '#suffix' => '</div>',
      '#wrapper_attributes' => [
        'class' => ['not_hover'],
        'width' => '50%',
      ],
    ];
    if ($this->isTemplate) {
      $form['users']['']['recipient']['text'] = [
        '#markup' => $this->novaPoshta->trans('The data is filled individually'),
      ];
    }
    else {
      $this->setAddress($form['users']['']['recipient'], $form_state, $this->recipientAjax);
      switch ($form_state->getValue('ServiceType')) {
        case'WarehouseDoors':
          $form['users']['']['recipient']['RecipientType'] = [
            '#type' => 'select',
            '#title' => $this->novaPoshta->trans('Recipient type'),
            '#options' => $this->api->getTypesOfCounterparties(),
            '#required' => TRUE,
            '#default_value' => $form_state
              ->getValue(['recipient', 'RecipientType']),
          ];
          $form['users']['']['recipient']['RecipientLastName'] = [
            '#type' => 'textfield',
            '#title' => $this->novaPoshta->trans('Last name'),
            '#required' => TRUE,
            '#default_value' => $form_state
              ->getValue(['recipient', 'RecipientLastName']),
          ];
          $form['users']['']['recipient']['RecipientFirstName'] = [
            '#type' => 'textfield',
            '#title' => $this->novaPoshta->trans('Name'),
            '#required' => TRUE,
            '#default_value' => $form_state
              ->getValue(['recipient', 'RecipientFirstName']),
          ];
          $form['users']['']['recipient']['RecipientMiddleName'] = [
            '#type' => 'textfield',
            '#title' => $this->novaPoshta->trans('Middle name'),
            '#required' => TRUE,
            '#default_value' => $form_state
              ->getValue(['recipient', 'RecipientMiddleName']),
          ];
          $form['users']['']['recipient']['RecipientsPhone'] = [
            '#type' => 'textfield',
            '#title' => $this->novaPoshta->trans('Recipient\'s phone'),
            '#required' => TRUE,
            '#attributes' => [
              'data-mask' => $this->novaPoshta::PHONE_MASK,
              'placeholder' => str_replace(9, '_', $this->novaPoshta::PHONE_MASK),
            ],
            '#default_value' => $form_state
              ->getValue(['recipient', 'RecipientsPhone']),
          ];
          break;

        default:
          $form['users']['']['recipient']['Recipient'] = [
            '#type' => 'select',
            '#title' => $this->novaPoshta->trans('Recipient'),
            '#options' => $this->api->getCounterparties([
              'CounterpartyProperty' => 'Recipient',
            ]),
            '#required' => TRUE,
            '#ajax' => $this->recipientAjax,
            '#field_suffix' => [
              '#type' => 'inline_template',
              '#template' => '<a href="javascript:void(0);" class="button--link target" onclick="{{ onclick }}" data-post="{{ post }}">{{ text }}</a>',
              '#context' => [
                'text' => $this->novaPoshta->trans('Create'),
                'onclick' => 'basket_admin_ajax_link(this, \'' . Url::fromRoute('basket.admin.pages', [
                  'page_type' => 'api-novaposhta-createCounterparty',
                ])->toString() . '\')',
                'post' => json_encode(['CounterpartyProperty' => 'Recipient']),
              ],
            ],
            '#default_value' => $form_state->getValue(['recipient', 'Recipient']),
          ];
          $recipientRef = $form_state->getValue(['recipient', 'Recipient']);
          if (!empty($recipientRef) && empty($form['users']['']['recipient']['Recipient']['#options'][$recipientRef])) {
            $recipientRef = NULL;
          }
          $options = [];
          if ($form_state->getValue(['recipient', 'RecipientsPhone'])) {
            $contact = $this->api->getCounterpartyContactPersons([
              'Ref' => $recipientRef,
            ], FALSE, $form_state->getValue(['recipient', 'RecipientsPhone']));
            foreach ($contact['data'] as $item) {
              if (!empty($contact['data'][0]['Ref'])) {
                $options[$item['Ref']] = $item['LastName'] . ' ' . $item['FirstName'];
                $phone = $item['Phones'];
              }
            }
          }
          if (empty($options)) {
            $options = $this->api->getCounterpartyContactPersons(['Ref' => $recipientRef]);
          }
          $form['users']['']['recipient']['ContactRecipient'] = [
            '#type' => 'select',
            '#title' => $this->novaPoshta->trans('Contact person'),
            '#options' => $options,
            '#empty_option' => t('- Select -'),
            '#required' => TRUE,
            '#ajax' => $this->recipientAjax,
            '#default_value' => $form_state
              ->getValue(['recipient', 'ContactRecipient']),
          ];
          if (!empty($recipientRef)) {
            $form['users']['']['recipient']['ContactRecipient']['#field_suffix'] = [
              '#type' => 'inline_template',
              '#template' => '<a href="javascript:void(0);" class="button--link target" onclick="{{ onclick }}" data-post="{{ post }}">{{ text }}</a>',
              '#context' => [
                'text' => $this->novaPoshta->trans('Create'),
                'onclick' => 'basket_admin_ajax_link(this, \'' . Url::fromRoute('basket.admin.pages', [
                  'page_type' => 'api-novaposhta-createContactRecipient',
                ])->toString() . '\')',
                'post' => json_encode(['CounterpartyRef' => $recipientRef]),
              ],
            ];
          }
          $recipientsPhones = [];
          $recipientRef = $form_state->getValue(['recipient', 'Recipient']);
          $contactRecipient = $form_state
            ->getValue(['recipient', 'ContactRecipient']);
          if (!empty($recipientRef) && !empty($contactRecipient)) {
            $recipientsPhones = $this->api->getCounterpartyContactPersons([
              'Ref' => $recipientRef,
              'Contact' => $contactRecipient,
            ]);
          }
          if (!empty($phone)) {
            $recipientsPhones[$phone] = $phone;
          }
          $form['users']['']['recipient']['RecipientsPhone'] = [
            '#type' => 'select',
            '#title' => $this->novaPoshta->trans('Recipient\'s phone'),
            '#options' => $recipientsPhones,
            '#required' => TRUE,
            '#empty_option' => t('- Select -'),
            '#ajax' => $this->recipientAjax,
            '#default_value' => $form_state
              ->getValue(['recipient', 'RecipientsPhone']),
          ];
          $recipientsPhone = $form_state
            ->getValue(['recipient', 'RecipientsPhone']);
          if (!empty($recipientsPhone) && !empty($recipientRef)) {
            if ($recipientsPhone == 'PhoneCustom') {
              $form['users']['']['recipient']['RecipientsPhoneCustom'] = [
                '#type' => 'textfield',
                '#title' => $this->novaPoshta->trans('Recipient\'s phone'),
                '#required' => TRUE,
                '#attributes' => [
                  'data-mask' => $this->novaPoshta::PHONE_MASK,
                  'placeholder' => str_replace(9, '_', $this->novaPoshta::PHONE_MASK),
                ],
                '#default_value' => $form_state
                  ->getValue(['recipient', 'RecipientsPhoneCustom']),
              ];
            }
          }
          break;
      }
    }

    $form['all'] = [
      '#type' => 'table',
      '#header' => [
        $this->novaPoshta->trans('Parameters of departure'),
        $this->novaPoshta->trans('Payment Information'),
      ],
      '' => [],
    ];
    $form['all']['']['parameters'] = [
      '#wrapper_attributes' => [
        'class' => ['not_hover'],
        'width' => '50%',
      ],
      'Cargo' => [
        '#parents' => [],
        '#prefix' => '<div id="' . $this->cargoAjax['wrapper'] . '">',
        '#suffix' => '</div>',
        'CargoType' => [
          '#type' => 'select',
          '#title' => $this->novaPoshta->trans('Type of cargo'),
          '#options' => $this->api->getCargoTypes(),
          '#required' => TRUE,
          '#ajax' => $this->cargoAjax,
          '#default_value' => $form_state->getValue('CargoType'),
        ],
      ],
    ];

    switch ($form_state->getValue('CargoType')) {
      case'Parcel':
      case'Cargo':
        $form['all']['']['parameters']['Cargo']['Weight'] = [
          '#type' => 'number',
          '#title' => $this->novaPoshta->trans('Total weight'),
          '#min' => 0.1,
          '#step' => 0.1,
          '#field_suffix' => $this->novaPoshta->trans('kg'),
          '#required' => TRUE,
          '#default_value' => $form_state->getValue('Weight'),
        ];
        $form['all']['']['parameters']['Cargo']['SeatsAmount'] = [
          '#type' => 'number',
          '#title' => $this->novaPoshta->trans('Number of seats'),
          '#min' => 1,
          '#step' => 1,
          '#required' => TRUE,
          '#default_value' => $form_state->getValue('SeatsAmount'),
        ];
        break;

      case'Documents':
        $form['all']['']['parameters']['Cargo']['Weight'] = [
          '#type' => 'select',
          '#title' => $this->novaPoshta->trans('Total weight'),
          '#options' => [
            '0.1' => '0.1',
            '0.5' => '0.5',
            '1.0' => '1.0',
          ],
          '#field_suffix' => $this->novaPoshta->trans('kg'),
          '#required' => TRUE,
          '#default_value' => $form_state->getValue('Weight'),
        ];
        $form['all']['']['parameters']['Cargo']['SeatsAmount'] = [
          '#type' => 'number',
          '#title' => $this->novaPoshta->trans('Number of seats'),
          '#min' => 1,
          '#step' => 1,
          '#required' => TRUE,
          '#default_value' => $form_state->getValue('SeatsAmount'),
        ];
        break;
    }
    $form['all']['']['payment'] = [
      '#wrapper_attributes' => [
        'class' => ['not_hover'],
        'width' => '50%',
      ],
      '#parents' => [],
      'Cost' => [
        '#type' => 'number',
        '#title' => $this->novaPoshta->trans('Announced price'),
        '#required' => TRUE,
        '#min' => 1,
        '#step' => 1,
        '#field_suffix' => $this->novaPoshta->trans('UAH'),
        '#default_value' => $form_state->getValue('Cost'),
      ],
      'BackwardDelivery' => [
        '#prefix' => '<div id="' . $this->backwardDelivery['wrapper'] . '">',
        '#suffix' => '</div>',
        'on' => [
          '#type'  => 'checkbox',
          '#title' => $this->novaPoshta->trans('Cash on delivery'),
          '#ajax' => $this->backwardDelivery,
          '#default_value' => $form_state->getValue(['BackwardDelivery', 'on']),
        ],
      ],
      'AfterpaymentOnGoodsCost' => [
        '#prefix' => '<div id="' . $this->afterpaymentOnGoodsCost['wrapper'] . '">',
        '#suffix' => '</div>',
        'on' => [
          '#type' => 'checkbox',
          '#title' => $this->novaPoshta->trans('Payment control'),
          '#ajax' => $this->afterpaymentOnGoodsCost,
          '#default_value' => $form_state
            ->getValue(['AfterpaymentOnGoodsCost', 'on']),
        ],
      ],
      'Description' => [
        '#type' => 'textfield',
        '#title' => $this->novaPoshta->trans('Description of departure'),
        '#required' => TRUE,
        '#default_value' => $form_state->getValue('Description'),
      ],
      'DateTime' => [
        '#type' => 'date',
        '#title' => $this->novaPoshta->trans('Date of dispatch'),
        '#required' => TRUE,
        '#default_value' => $form_state->getValue('DateTime'),
      ],
      'PayerType' => [
        '#type' => 'select',
        '#title' => $this->novaPoshta->trans('Payer'),
        '#options' => $this->api->getTypesOfPayers([]),
        '#required' => TRUE,
        '#default_value' => $form_state->getValue('PayerType'),
      ],
      'PaymentMethod' => [
        '#type' => 'select',
        '#title' => $this->novaPoshta->trans('Form of payment'),
        '#options' => $this->api->getPaymentForms([]),
        '#required' => TRUE,
        '#default_value' => $form_state->getValue('PaymentMethod'),
      ],
      'InfoRegClientBarcodes' => [
        '#type' => 'textfield',
        '#title' => $this->novaPoshta->trans('Internal number'),
        '#default_value' => $form_state->getValue('InfoRegClientBarcodes'),
      ],
    ];
    if (!empty($this->orderId)) {
      $form['all']['']['payment']['InfoRegClientBarcodes']['#value'] = $this->orderId;
      $form['all']['']['payment']['InfoRegClientBarcodes']['#disabled'] = TRUE;
    }
    if (!empty($form_state->getValue(['BackwardDelivery', 'on']))) {
      $form['all']['']['payment']['BackwardDelivery']['amount'] = [
        '#type' => 'number',
        '#title' => $this->novaPoshta->trans('Cash on delivery amount'),
        '#required' => TRUE,
        '#min' => 0,
        '#step' => 0.01,
        '#field_suffix' => $this->novaPoshta->trans('UAH'),
        '#default_value' => $form_state->getValue(['BackwardDelivery', 'amount']),
      ];
      if (!empty($form_state->getValue(['BackwardDelivery', 'disabled']))) {
        $form['all']['']['payment']['BackwardDelivery']['amount']['#disabled'] = TRUE;
        $form['all']['']['payment']['BackwardDelivery']['on']['#disabled'] = TRUE;
      }
    }
    if (!empty($form_state->getValue(['AfterpaymentOnGoodsCost', 'on']))) {
      $form['all']['']['payment']['AfterpaymentOnGoodsCost']['amount'] = [
        '#type' => 'number',
        '#title' => $this->novaPoshta->trans('Summ'),
        '#required' => TRUE,
        '#min' => 0,
        '#step' => 0.01,
        '#field_suffix' => $this->novaPoshta->trans('UAH'),
        '#default_value' => $form_state
          ->getValue(['AfterpaymentOnGoodsCost', 'amount']),
      ];
      if (!empty($form_state->getValue(['AfterpaymentOnGoodsCost', 'disabled']))) {
        $form['all']['']['payment']['AfterpaymentOnGoodsCost']['amount']['#disabled'] = TRUE;
        $form['all']['']['payment']['AfterpaymentOnGoodsCost']['on']['#disabled'] = TRUE;
      }
    }
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#name' => 'insert',
        '#value' => $this->novaPoshta->trans('Create invoice'),
        '#validate' => [[$this, 'insertValid']],
      ],
    ];
    if ($this->isTemplate) {
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#name' => 'saveTemplate',
        '#value' => $this->novaPoshta->trans('Save template'),
      ];
    }
    if (!empty($this->editEN)) {
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#name' => 'update',
        '#value' => $this->novaPoshta->trans('Update invoice'),
        '#validate' => [
          [$this, 'validatePaymentOptions'],
          [$this, 'insertValid'],
        ],
      ];
    }

    $recipientPoint = $form_state->getValue(['recipient', 'point']);
    if ($form_state->getValue('CargoType') != 'Documents') {
      if (!empty($recipientPoint) && !empty($form['users']['']['recipient']['point']['#options'])) {
        if (str_contains($form['users']['']['recipient']['point']['#options'][$recipientPoint], 'Поштомат')) {
          $form['all']['']['parameters']['Cargo']['OptionsSeat'] = [
            '#type' => 'select',
            '#title' => $this->novaPoshta->trans('Cell'),
            '#required' => TRUE,
            '#options' => $this->novaPoshta->getOptionsSeat(),
          ];
        }
      }
    }

    return $form;
  }

  /**
   * Validates the payment options selected in the form.
   *
   * @param array $form
   *   The render array of the form being validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validatePaymentOptions(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue(['BackwardDelivery', 'on']))
      && !empty($form_state->getValue(['AfterpaymentOnGoodsCost', 'on']))) {
      $form_state->setErrorByName('AfterpaymentOnGoodsCost][on', $this->novaPoshta->trans("'Cash on delivery' and 'Payment control' are not allowed at the same time"));
    }
  }

  /**
   * Removes a specified template from the form and configuration.
   *
   * @param array $form
   *   The render array of the form where the template is managed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function removeTemplate(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if (!empty($triggering_element['#template_name'])) {
      $templateName = $triggering_element['#template_name'];
      $config = \Drupal::configFactory()->getEditable('novaposhta.en.template');
      $templates = $config->get('config.sender.templates');
      if (!empty($templates[$templateName])) {
        unset($templates[$templateName]);
      }
      $config->set('config.sender.templates', $templates);
      $config->save();
      unset($form['users']['']['sender']['template']['#options'][$templateName]);
      unset($form['users']['']['sender']['template_name']);
      unset($form['users']['']['sender']['template_remove']);
      $form_state->setRebuild();
    }
    return $form['users']['']['sender'];
  }

  /**
   * Handles the submission of the form based on the triggering element.
   *
   * @param array $form
   *   The render array of the form being submitted.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the submitted form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggerElement = $form_state->getTriggeringElement();
    if (!empty($triggerElement['#name'])) {
      switch ($triggerElement['#name']) {
        case'saveTemplate':
          $values = $form_state->getValues();
          foreach ($values as $key => $value) {
            if (in_array($key, [
              'submit',
              'form_build_id',
              'form_token',
              'form_id',
              'saveTemplate',
              'ajaxSetValue',
            ])) {
              unset($values[$key]);
            }
          }
          $templates = \Drupal::config('novaposhta.en.template')->get('config.sender.templates') ?? [];
          if (!empty($values['sender']['template']) && $values['sender']['template'] !== '_none' && !empty($values['sender']['template_name'])) {
            unset($values['sender']['template']);
            $templates[$values['sender']['template_name']] = $values['sender'];
            $values['sender'] = \Drupal::config('novaposhta.en.template')->get('config.sender');
          }
          $values['sender']['templates'] = $templates;
          if (isset($values['sender']['template'])) {
            unset($values['sender']['template']);
          }

          // Save configuration:
          $config = \Drupal::configFactory()->getEditable('novaposhta.en.template');
          $config->set('config', $values);
          $config->save();
          $this->messenger->addMessage($this->novaPoshta->t('The configuration options have been saved.'), 'status');
          break;

        case'insert':
          if (!empty($intDocNumber = $form_state->get('IntDocNumber'))) {
            $form_state->setRedirect('basket.admin.pages', [
              'page_type' => 'novaposhta',
            ], [
              'query' => [
                'viewEn' => $intDocNumber,
              ],
            ]);
          }
          break;
      }
    }
  }

  /**
   * Processes the form submission when the form state.
   *
   * @param array $form
   *   The render array of the form being processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the submitted form.
   */
  public function insertValid(array $form, FormStateInterface $form_state) {
    if ($form_state->hasAnyErrors()) {
      return;
    }
    $triggerElement = $form_state->getTriggeringElement();
    if (!empty($triggerElement['#name'])) {
      switch ($triggerElement['#name']) {
        case'update':
        case'insert':
          $this->novaPoshta->eN()->setEn($form_state, $this->editEN);
          break;
      }
    }
  }

  /**
   * Configures the address form elements based.
   *
   * @param array $form
   *   The render array of the form that needs address modifications.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $ajax
   *   The AJAX properties used for dynamic updates of the form elements.
   */
  public function setAddress(array &$form, FormStateInterface $form_state, array $ajax) {
    $key = $form['#parents'][0];
    $serviceTypeKey = $form_state->getValue('ServiceType') . '_' . $key;
    switch ($serviceTypeKey) {
      case'WarehouseDoors_recipient':
        $form['city'] = [
          '#type' => 'textfield',
          '#title' => $this->novaPoshta->trans('City'),
          '#required' => TRUE,
          '#default_value' => $form_state->getValue([$key, 'city']),
        ];
        $form['street'] = [
          '#type' => 'textfield',
          '#title' => $this->novaPoshta->trans('Street'),
          '#required' => TRUE,
          '#default_value' => $form_state->getValue([$key, 'street']),
        ];
        $form['house'] = [
          '#type' => 'textfield',
          '#title' => $this->novaPoshta->trans('House number'),
          '#required' => TRUE,
          '#default_value' => $form_state->getValue([$key, 'house']),
        ];
        break;

      default:
        $form['region'] = [
          '#type' => 'select',
          '#title' => $this->novaPoshta->trans('Region'),
          '#options' => $this->api->getRegions([]),
          '#ajax' => $ajax,
          '#required' => TRUE,
          '#attributes' => [
            'class' => [$this->jQueryStyleID . '_np_select'],
            'data-min' => 0,
          ],
        ];
        if (!empty($region = $form_state->getValue([$key, 'region']))) {
          if (!empty($form['region']['#options'][$region])) {
            $form['region']['#default_value'] = $region;
          }
          else {
            $form_state->setValue([$key, 'region'], NULL);
          }
        }
        $form['city'] = [
          '#type' => 'select',
          '#title' => $this->novaPoshta->trans('City'),
          '#options' => $this->api->getCitis([
            'region' => $form_state->getValue([$key, 'region']),
          ]),
          '#ajax' => $ajax,
          '#required' => TRUE,
          '#attributes' => [
            'class' => [$this->jQueryStyleID . '_np_select'],
            'data-min' => 0,
          ],
        ];
        if (!empty($city = $form_state->getValue([$key, 'city']))) {
          if (!empty($form['city']['#options'][$city])) {
            $form['city']['#default_value'] = $city;
          }
          else {
            $form_state->setValue([$key, 'city'], NULL);
          }
        }
        $form['point'] = [
          '#type' => 'select',
          '#title' => $this->novaPoshta->trans('Point'),
          '#options' => $this->api->getPoints([
            'city' => $form_state->getValue([$key, 'city']),
          ]),
          '#ajax' => [
            'wrapper' => 'novaposhta_en_form_ajax_wrap',
            'callback' => [$this, 'ajaxReload'],
          ],
          '#required' => TRUE,
          '#attributes' => [
            'class' => [$this->jQueryStyleID . '_np_select'],
            'data-min' => 0,
          ],
        ];
        if (!empty($point = $form_state->getValue([$key, 'point']))) {
          if (!empty($form['point']['#options'][$point])) {
            $form['point']['#default_value'] = $point;
          }
          else {
            $form_state->setValue([$key, 'point'], NULL);
          }
        }
        break;
    }
  }

  /**
   * Reloads the form via AJAX and returns the updated form structure.
   *
   * @param array $form
   *   The render array of the form to be reloaded.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function ajaxReload(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Reloads the recipient field in the form using AJAX.
   *
   * @param array $form
   *   The form structure array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function ajaxRecipientReload(array $form, FormStateInterface $form_state) {
    return $form['users']['']['recipient'];
  }

  /**
   * Reloads and returns the sender form element.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function ajaxSenderReload(array $form, FormStateInterface $form_state) {
    return $form['users']['']['sender'];
  }

  /**
   * Reloads the cargo parameters section of the form via AJAX.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function ajaxCargoReload(array $form, FormStateInterface $form_state) {
    return $form['all']['']['parameters']['Cargo'];
  }

  /**
   * Reloads the Backward Delivery section of the payment form.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function ajaxBackwardDeliveryReload(array $form, FormStateInterface $form_state) {
    return $form['all']['']['payment']['BackwardDelivery'];
  }

  /**
   * Reloads the afterpayment cost section of the goods payment form via AJAX.
   *
   * @param array $form
   *   The entire form structure passed to the method.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function ajaxAfterpaymentOnGoodsCostReload(array $form, FormStateInterface $form_state) {
    return $form['all']['']['payment']['AfterpaymentOnGoodsCost'];
  }

  /**
   * Updates the state of a form with new values.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public static function changeFormState(FormStateInterface $form_state) {
    if (!empty($setValue = $form_state->getValue('ajaxSetValue'))) {
      $setValues = json_decode(trim($setValue), TRUE);
      if (!empty($setValues)) {
        foreach ($setValues as $setValue) {
          $form_state->setValue($setValue['fieldName'], $setValue['value']);
        }
      }
      $form_state->setValue('ajaxSetValue', '');
      $values = &$form_state->getValues();
      $userInput = &$form_state->getUserInput();
      $userInput = $values;
    }
  }

}
