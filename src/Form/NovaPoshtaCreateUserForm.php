<?php

namespace Drupal\novaposhta\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\novaposhta\API\NovaPoshtaAPI;

/**
 * Class of NovaPoshtaCreateUserForm.
 */
class NovaPoshtaCreateUserForm extends FormBase {

  /**
   * NovaPoshta object.
   *
   * @var \Drupal\novaposhta\NovaPoshta|object|null
   */
  protected $novaPoshta;

  /**
   * CounterpartyProperty id.
   *
   * @var mixed|null
   */
  protected $counterpartyProperty;

  /**
   * NovaPoshtaAPI object.
   *
   * @var \Drupal\novaposhta\API\NovaPoshtaAPI
   */
  protected $api;

  /**
   * Constructs a new instance of the class.
   */
  public function __construct($counterpartyProperty = NULL) {
    $this->novaPoshta = \Drupal::getContainer()->get('NovaPoshta');
    $this->counterpartyProperty = $counterpartyProperty;
    $this->api = new NovaPoshtaAPI(\Drupal::config('novaposhta.settings')->get('config.api_key'));
  }

  /**
   * Retrieves the unique identifier for the form.
   */
  public function getFormId() {
    return 'novaposhta_create_user_form';
  }

  /**
   * Builds a form for creating or editing a counterparty.
   *
   * @param array $form
   *   A structured array containing the initial form elements and
   *   their properties.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An object that holds the current state of the form, including values and
   *   user input.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form += [
      '#prefix' => '<div id="novaposhta_create_user_form_ajax_wrap">',
      '#suffix' => '</div>',
      'status_messages' => [
        '#type' => 'status_messages',
      ],
    ];
    $form['CounterpartyProperty'] = [
      '#type' => 'hidden',
      '#value' => $this->counterpartyProperty,
    ];
    $form['CounterpartyType'] = [
      '#type' => 'select',
      '#title' => $this->novaPoshta->trans('Counterparty type'),
      '#options' => $this->api->getTypesOfCounterparties(),
      '#required' => TRUE,
      '#default_value' => 'PrivatePerson',
      '#ajax' => [
        'wrapper' => 'novaposhta_create_user_form_ajax_wrap',
        'callback' => [$this, 'ajaxReload'],
      ],
    ];
    $counterpartyType = $form_state->getValue('CounterpartyType');
    if ($counterpartyType == 'Organization') {
      $form['OwnershipForm'] = [
        '#type' => 'select',
        '#title' => $this->novaPoshta->trans('Form of ownership'),
        '#options' => $this->api->getOwnershipFormsList(),
        '#required' => TRUE,
      ];
      $form['FirstName'] = [
        '#type' => 'textfield',
        '#title' => $this->novaPoshta->trans('Organization name'),
        '#required' => TRUE,
      ];
    }
    else {
      $form['LastName'] = [
        '#type' => 'textfield',
        '#title' => $this->novaPoshta->trans('Surname'),
        '#required' => TRUE,
      ];
      $form['FirstName'] = [
        '#type' => 'textfield',
        '#title' => $this->novaPoshta->trans('Name'),
        '#required' => TRUE,
      ];
      $form['MiddleName'] = [
        '#type' => 'textfield',
        '#title' => $this->novaPoshta->trans('Middle name'),
      ];
      $form['Phone'] = [
        '#type' => 'textfield',
        '#title' => $this->novaPoshta->trans('Phone'),
        '#required' => TRUE,
        '#attributes' => [
          'data-mask' => $this->novaPoshta::PHONE_MASK,
          'placeholder' => str_replace(9, '_', $this->novaPoshta::PHONE_MASK),
        ],
      ];
      $form['Email'] = [
        '#type' => 'email',
        '#title' => $this->novaPoshta->trans('E-mail'),
      ];
    }
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->novaPoshta->trans('Create'),
        '#name' => 'save',
        '#ajax' => [
          'wrapper' => 'novaposhta_create_user_form_ajax_wrap',
          'callback' => [$this, 'ajaxReload'],
        ],
      ],
    ];
    return $form;
  }

  /**
   * Handles the submission of a form.
   *
   * @param array $form
   *   A structured array containing the form elements and their values.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An object representing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Processes an AJAX form submission.
   *
   * @param array $form
   *   A structured array containing the form elements and data.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An object containing the current state of the form.
   */
  public function ajaxReload(array &$form, FormStateInterface $form_state) {
    $triggerElement = $form_state->getTriggeringElement();
    if (!empty($triggerElement['#name']) && $triggerElement['#name'] == 'save' && !$form_state->getErrors()) {
      $values = $form_state->getValues();
      $fields = [
        'CounterpartyType' => $values['CounterpartyType'],
        'CounterpartyProperty' => $values['CounterpartyProperty'],
      ];
      switch ($fields['CounterpartyType']) {
        case'Organization':
          $fields['OwnershipForm'] = $values['OwnershipForm'];
          $fields['FirstName'] = trim($values['FirstName']);
          break;

        case'PrivatePerson':
          $fields['LastName'] = !empty($values['LastName']) ? trim($values['LastName']) : '';
          $fields['FirstName'] = !empty($values['FirstName']) ? trim($values['FirstName']) : '';
          $fields['MiddleName'] = !empty($values['MiddleName']) ? trim($values['MiddleName']) : '';
          $fields['Phone'] = !empty($values['Phone']) ? $this->novaPoshta->replacePhone(trim($values['Phone'])) : '';
          $fields['Email'] = !empty($values['Email']) ? trim($values['Email']) : '';
          break;
      }
      if (!empty($fields)) {
        $result = $this->api->saveCounterparty($fields);
        if (!empty($result['errors'])) {
          foreach ($result['errors'] as $error) {
            \Drupal::messenger()->addMessage($error, 'error');
          }
        }
        if (!empty($result['success'])) {
          $this->api->getCounterparties([
            'CounterpartyProperty' => $fields['CounterpartyProperty'],
          ], TRUE);
          $response = new AjaxResponse();
          $form = [
            '#markup' => $this->novaPoshta->trans('Counterparty successfully created'),
          ];
          $response->addCommand(new ReplaceCommand('#novaposhta_create_user_form_ajax_wrap', $form));
          $setValues = [
            [
              'fieldName' => ['recipient', 'Recipient'],
              'value' => $result['data'][0]['Ref'],
            ],
          ];
          if (!empty($result['data'][0]['ContactPerson']['data'][0]['Ref'])) {
            $setValues[] = [
              'fieldName' => ['recipient', 'ContactRecipient'],
              'value' => $result['data'][0]['ContactPerson']['data'][0]['Ref'],
            ];
            $this->api->getCounterpartyContactPersons([
              'Ref' => $result['data'][0]['Ref'],
            ], TRUE);
          }
          $response->addCommand(new InvokeCommand('[name="ajaxSetValue"]', 'val', [json_encode($setValues)]));
          $response->addCommand(new InvokeCommand('[name="ajaxSetValue"]', 'change', []));
          return $response;
        }
      }
    }
    return $form;
  }

}
