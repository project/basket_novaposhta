<?php

namespace Drupal\novaposhta\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\novaposhta\API\NovaPoshtaAPI;

/**
 * Class of NovaPoshtaCreateContactPersonForm.
 */
class NovaPoshtaCreateContactPersonForm extends FormBase {

  /**
   * NovaPoshta object.
   *
   * @var \Drupal\novaposhta\NovaPoshta|object|null
   */
  protected $novaPoshta;

  /**
   * CounterpartyRef ID.
   *
   * @var mixed|null
   */
  protected $counterpartyRef;

  /**
   * NovaPoshtaAPI object.
   *
   * @var \Drupal\novaposhta\API\NovaPoshtaAPI
   */
  protected $api;

  /**
   * Constructs a new instance of the class.
   */
  public function __construct($counterpartyRef = NULL) {
    $this->novaPoshta = \Drupal::getContainer()->get('NovaPoshta');
    $this->counterpartyRef = $counterpartyRef;
    $this->api = new NovaPoshtaAPI(\Drupal::config('novaposhta.settings')->get('config.api_key'));
  }

  /**
   * Retrieves the unique identifier for the form.
   */
  public function getFormId() {
    return 'novaposhta_create_contact_person_form';
  }

  /**
   * Builds the contact person creation form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form += [
      '#prefix' => '<div id="novaposhta_create_contact_person_form_ajax_wrap">',
      '#suffix' => '</div>',
      'status_messages' => [
        '#type' => 'status_messages',
      ],
    ];
    $form['CounterpartyRef'] = [
      '#type' => 'hidden',
      '#value' => $this->counterpartyRef,
    ];
    $form['LastName'] = [
      '#type' => 'textfield',
      '#title' => $this->novaPoshta->trans('Surname'),
      '#required' => TRUE,
    ];
    $form['FirstName'] = [
      '#type' => 'textfield',
      '#title' => $this->novaPoshta->trans('Name'),
      '#required' => TRUE,
    ];
    $form['MiddleName'] = [
      '#type' => 'textfield',
      '#title' => $this->novaPoshta->trans('Middle name'),
    ];
    $form['Phone'] = [
      '#type' => 'textfield',
      '#title' => $this->novaPoshta->trans('Phone'),
      '#required' => TRUE,
      '#attributes' => [
        'data-mask' => $this->novaPoshta::PHONE_MASK,
        'placeholder' => str_replace(9, '_', $this->novaPoshta::PHONE_MASK),
      ],
    ];
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->novaPoshta->trans('Create'),
        '#name' => 'save',
        '#ajax' => [
          'wrapper' => 'novaposhta_create_contact_person_form_ajax_wrap',
          'callback' => [$this, 'ajaxReload'],
        ],
      ],
    ];
    return $form;
  }

  /**
   * Processes the form submission.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Handles the AJAX form submission.
   */
  public function ajaxReload(array &$form, FormStateInterface $form_state) {
    $triggerElement = $form_state->getTriggeringElement();
    if (!empty($triggerElement['#name']) && $triggerElement['#name'] == 'save' && !$form_state->getErrors()) {
      $values = $form_state->getValues();
      $fields = [
        'CounterpartyRef' => !empty($values['CounterpartyRef']) ? trim($values['CounterpartyRef']) : '',
        'FirstName' => !empty($values['FirstName']) ? trim($values['FirstName']) : '',
        'LastName' => !empty($values['LastName']) ? trim($values['LastName']) : '',
        'MiddleName' => !empty($values['MiddleName']) ? trim($values['MiddleName']) : '',
        'Phone' => !empty($values['Phone']) ? $this->novaPoshta->replacePhone(trim($values['Phone'])) : '',
      ];
      $result = $this->api->saveContactPerson($fields);
      if (!empty($result['errors'])) {
        foreach ($result['errors'] as $error) {
          \Drupal::messenger()->addMessage($error, 'error');
        }
      }
      if (!empty($result['success'])) {
        $response = new AjaxResponse();
        \Drupal::getContainer()->get('BasketPopup')->openModal(
          $response,
          $this->novaPoshta->trans('Create a contact person'),
          [
            '#markup' => $this->novaPoshta->trans('Contact person created successfully'),
          ], [
            'width' => 400,
            'class' => ['novaposhta_popup'],
          ],
        );
        $this->api->getCounterpartyContactPersons([
          'Ref' => $values['CounterpartyRef'],
        ], TRUE);
        $setValues = [
          [
            'fieldName' => ['recipient', 'ContactRecipient'],
            'value' => $result['data'][0]['Ref'],
          ],
        ];
        $response->addCommand(new InvokeCommand('[name="ajaxSetValue"]', 'val', [json_encode($setValues)]));
        $response->addCommand(new InvokeCommand('[name="ajaxSetValue"]', 'change', []));
        return $response;
      }
    }
    return $form;
  }

}
