<?php

namespace Drupal\novaposhta;

use Drupal\basket\ViewsAlters as BasketViewsAlters;
use Drupal\Core\Render\Element;
use Drupal\novaposhta\API\NovaPoshtaAPI;

/**
 * Class of ViewsAlter.
 */
class ViewsAlter {

  /**
   * NovaPoshta object.
   *
   * @var \Drupal\novaposhta\NovaPoshta|object|null
   */
  protected $novaPoshta;

  /**
   * Basket object.
   *
   * @var \Drupal\basket\Basket|object|null
   */
  protected $basket;

  /**
   * Constructs a new instance of the class.
   *
   * This constructor initializes the necessary services, including Nova Poshta
   * and Basket, by retrieving them from the service container.
   */
  public function __construct() {
    $this->novaPoshta = \Drupal::getContainer()->get('NovaPoshta');
    $this->basket = \Drupal::getContainer()->get('Basket');
  }

  /**
   * Alters the variables for rendering a table in a Drupal View.
   *
   * This method adds custom modifications to the table header, rows, or caption
   * elements, based on the provided View configuration and results.
   */
  public function viewsViewTable(&$vars) {
    if (!empty($vars['header'])) {
      \Drupal::classResolver(BasketViewsAlters::class)->tableHeaderAlter($vars['header'], 'novaposhta');
    }
    if (empty($vars['view']->total_rows) && !empty($vars['rows'])) {
      $vars['rows'][0]['columns'][0]['content'][1]['field_output'] = [
        '#markup' => $this->novaPoshta->trans('The list is empty.'),
      ];
    }

    $res = $vars['view']->query->query()->execute()->fetchAll();
    $ids = [];
    if (!empty($res)) {
      foreach ($res as $re) {
        $ids[$re->np_en_num] = $re->np_en_num;
      }
    }
    if (!empty($ids)) {
      $vars['caption_needed'] = TRUE;
      $vars['caption'] = [
        '#type' => 'inline_template',
        '#template' => '<a href="{{ url }}" target="_blank" class="np-print-filter">{{ ico|raw }}</a>',
        '#context' => [
          'ico' => \Drupal::getContainer()->get('Basket')->getIco('print.svg', 'novaposhta'),
          'url' => (new NovaPoshtaAPI())->getPrintLink(implode(',', $ids)),
        ],
      ];
    }
  }

  /**
   * Alters the form elements by translating specific text values.
   *
   * @param array $form
   *   The form array being altered, passed by reference.
   */
  public function formAlter(array &$form) {
    if (!empty($form['actions']['submit']['#value'])) {
      $form['actions']['submit']['#value'] = $this->novaPoshta->trans(trim($form['actions']['submit']['#value']));
    }
    foreach (Element::children($form) as $fieldName) {
      if (!empty($form[$fieldName]['#attributes']['placeholder'])) {
        $form[$fieldName]['#attributes']['placeholder'] = $this->novaPoshta->trans(trim($form[$fieldName]['#attributes']['placeholder']));
      }
      if (!empty($form['#info']['filter-' . $fieldName]['label'])) {
        $form['#info']['filter-' . $fieldName]['label'] = $this->novaPoshta->trans(trim($form['#info']['filter-' . $fieldName]['label']));
      }
    }
  }

}
