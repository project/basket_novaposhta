<?php

namespace Drupal\novaposhta;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\novaposhta\API\NovaPoshtaAPI;
use Drupal\Core\File\FileSystemInterface;
use Drupal\novaposhta\API\NovaPoshtaApi2;

/**
 * Class of NovaPoshta.
 */
class NovaPoshta {

  use StringTranslationTrait;

  const NOVAPOCHTA_DIR = 'public://settings/novaposhta';
  const PHONE_MASK = '+38(099)999-99-99';
  const JQUERY_STYLE_DEF = 'chosen';

  /**
   * EN info.
   *
   * @var mixed
   */
  protected static $en;

  /**
   * NovaPoshtaEN object.
   *
   * @var mixed
   */
  protected static $novaPoshtaEN;

  const DEF_OPTIONS_SEAT = [
    ['36', '40', '58'],
    ['23', '40', '58'],
    ['15', '40', '58'],
  ];

  /**
   * Translates a given string with optional arguments and context.
   *
   * @deprecated in nova_poshta:1.x-1.0 and is removed from nova_poshta:2.x-2.0.
   *   Use ->trans($string, $args, $context) instead.
   * @see https://www.drupal.org/node/0
   */
  public function t($string, $args = [], $context = 'novaposhta') {
    return $this->trans($string, $args, $context);
  }

  /**
   * Translates a given string with optional arguments and context.
   */
  public function trans($string, $args = [], $context = 'novaposhta') {
    return $this->getStringTranslation()->translate($string, $args, ['context' => $context]);
  }

  /**
   * Retrieves the default options for seating.
   *
   * @return mixed
   *   The default seating options defined.
   */
  public function defOptionsSeat() {
    return $this::DEF_OPTIONS_SEAT;
  }

  /**
   * Executes the scheduled tasks for maintaining and updating Nova Poshta data.
   *
   * This method performs the following operations:
   * - Cleans up temporary directories older than a month.
   * - Updates the invoice list and document statuses.
   * - Updates area and city lists based on the configuration.
   */
  public function cronRun() {

    // Data cleansing:
    $tmpDir = $this::NOVAPOCHTA_DIR . '/' . date('W_m_Y', strtotime('-1 month'));
    if (is_dir($tmpDir)) {
      \Drupal::service('file_system')->deleteRecursive($tmpDir);
    }
    $this->clearTmp($tmpDir);

    if (empty($GLOBALS['config']['novaposhta']['disabled.cron.document.list'])) {

      // Update invoice list:
      $api = new NovaPoshtaAPI();
      $api->updateDocumentList([
        'GetFullList' => 1,
        'DateTimeFrom' => date('d.m.Y', strtotime('-' . NOVAPOSHTA_EN_DAYS . ' days')),
        'DateTimeTo' => date('d.m.Y'),
      ]);

      // Update status list:
      $enNums = \Drupal::database()->select('novaposhta_en', 'en')
        ->fields('en', ['en_num'])
        ->condition('en.delivery_day', time(), '>=')
        ->execute()->fetchCol();

      if (!empty($enNums)) {
        $api->getStatusDocuments($enNums);
      }
    }

    // Update lists:
    if (empty($GLOBALS['config']['novaposhta']['disabled.cron.area.list'])) {
      $this->runUpdate('area');
    }
    if (empty($GLOBALS['config']['novaposhta']['disabled.cron.city.list'])) {
      $this->runUpdate('city');
    }
  }

  /**
   * Clears the temporary directory.
   *
   * @param string|null $dir
   *   The path to the directory to be cleared.
   */
  public function clearTmp(string $dir = NULL) {
    if (empty($dir)) {
      $dir = $this::NOVAPOCHTA_DIR;
    }
    if (is_dir($dir)) {
      \Drupal::service('file_system')->deleteRecursive($dir);
    }
  }

  /**
   * Saves the given data to a file within a specified directory.
   *
   * @param array $data
   *   The data to be saved, provided as an array.
   * @param string $fileName
   *   The name of the file where the data will be stored.
   * @param string|null $dir
   *   The directory where the file will be stored. If not provided, a default
   *   directory will be used.
   */
  public function variableSet(array $data = [], string $fileName = '', string $dir = NULL) {
    if (empty($dir)) {
      $dir = $this::NOVAPOCHTA_DIR;
    }
    \Drupal::service('file_system')->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY);
    $path = $dir . '/' . $fileName;
    file_put_contents($path, json_encode($data));
  }

  /**
   * Retrieves and decodes a JSON file from a specified directory.
   *
   * @param string $fileName
   *   The name of the file to fetch.
   * @param string|null $dir
   *   The directory where the file is located.
   */
  public function variableGet(string $fileName = '', string $dir = NULL) {
    if (empty($dir)) {
      $dir = $this::NOVAPOCHTA_DIR;
    }
    $path = $dir . '/' . $fileName;
    if (file_exists($path)) {
      $file = file_get_contents($path);
    }
    return !empty($file) ? json_decode($file, TRUE) : [];
  }

  /**
   * Removes special characters from a phone number.
   *
   * @param string $phone
   *   The phone number to be sanitized.
   */
  public function replacePhone($phone) {
    return str_replace(['(', ')', '-', '+', ' '], '', $phone);
  }

  /**
   * Loads an entity from the 'novaposhta_en' table by its ID.
   *
   * @param int|string $viewId
   *   The unique identifier of the entity to load.
   */
  public function loadEnById($viewId) {
    if (!isset(self::$en[$viewId])) {
      self::$en[$viewId] = \Drupal::database()->select('novaposhta_en', 'n')
        ->fields('n')
        ->condition('n.id', $viewId)
        ->execute()->fetchObject();
    }
    return self::$en[$viewId];
  }

  /**
   * Loads an entry from the 'novaposhta_en' table by the given number.
   *
   * @param string $num
   *   The number used to identify and fetch the entry.
   */
  public function loadEnByNum(string $num) {
    return \Drupal::database()->select('novaposhta_en', 'n')
      ->fields('n')
      ->condition('n.en_num', $num)
      ->execute()->fetchObject();
  }

  /**
   * Deletes an entry from the 'novaposhta_en' table by its ID.
   *
   * @param int $id
   *   The ID of the entry to be deleted.
   */
  public function deleteById(int $id) {
    $en = $this->loadEnById($id);
    if (!empty($en)) {
      \Drupal::database()->delete('novaposhta_en')
        ->condition('id', $id)
        ->execute();
      if (!empty($en->ref)) {
        $api = new NovaPoshtaAPI();
        $api->internetDocumentDelete([
          'DocumentRefs' => $en->ref,
        ]);
      }
    }
  }

  /**
   * Retrieves or initializes an instance of NovaPoshtaEN for the given API key.
   *
   * @param string|null $apiKEY
   *   The API key used to initialize the NovaPoshtaEN instance.
   */
  public function eN(string $apiKEY = NULL) {
    if (!isset(self::$novaPoshtaEN[$apiKEY])) {
      self::$novaPoshtaEN[$apiKEY] = new NovaPoshtaEN($apiKEY);
    }
    return self::$novaPoshtaEN[$apiKEY];
  }

  /**
   * Retrieves the jQuery style ID from the configuration.
   */
  public function getJqueryStyleId() {
    $jQueryStyleID = \Drupal::config('novaposhta.settings')->get('config.jquery_style');
    if (empty($jQueryStyleID)) {
      $jQueryStyleID = $this::JQUERY_STYLE_DEF;
    }
    return $jQueryStyleID;
  }

  /**
   * Attaches jQuery styling libraries and settings to a form.
   *
   * @param array $form
   *   The form array to which jQuery libraries and settings will be attached.
   */
  public function jQuerySelectAttached(array &$form) {
    $form['#attached']['drupalSettings']['no_results_text'] = $this->trans('No matches');
    $form['#attached']['drupalSettings']['search_text'] = $this->trans('Enter search data');
    switch ($this->getJqueryStyleId()) {
      case'chosen':
        $form['#attached']['library'][] = 'novaposhta/chosen';
        break;

      case'select2':
        $form['#attached']['library'][] = 'webform/webform.element.select2';
        $form['#attached']['library'][] = 'novaposhta/np';
        break;
    }
  }

  /**
   * Updates data in the 'novaposhta_lists' table based on the specified type.
   *
   * @param string $type
   *   The type of data to update. Supported values are 'area' and 'city'.
   *   - 'area': Fetches and updates area data.
   *   - 'city': Fetches and updates city data.
   */
  public function runUpdate(string $type = '') {
    $api = new NovaPoshtaApi2(NULL);
    switch ($type) {
      case'area':
        $areas = $api->getAreas();
        if (!empty($areas['data'])) {
          foreach ($areas['data'] as $area) {
            \Drupal::database()->merge('novaposhta_lists')
              ->keys([
                'ref_id' => $area['Ref'],
                'type' => 'area',
              ])
              ->fields([
                'name' => trim($area['Description']),
                'last_update' => time(),
              ])
              ->execute();
          }
        }
        break;

      case'city':
        $cities = $api->getCities(0, '', NULL);
        if (!empty($cities['data'])) {
          foreach ($cities['data'] as $city) {
            switch ($city['SettlementTypeDescription']) {
              case'село':
                $city['SettlementTypeDescription'] = 'с';
                break;

              case'місто':
                $city['SettlementTypeDescription'] = 'м';
                break;

              case'селище міського типу':
                $city['SettlementTypeDescription'] = 'смт.';
                break;

              case'селище':
                $city['SettlementTypeDescription'] = 'сел';
                break;
            }
            $names = [
              $city['SettlementTypeDescription'] ?? NULL,
              $city['Description'],
            ];

            $name = implode('. ', array_filter($names));
            \Drupal::database()->merge('novaposhta_lists')
              ->keys([
                'ref_id' => $city['Ref'],
                'type' => 'city',
              ])
              ->fields([
                'parent_id' => $city['Area'],
                'name' => trim($name),
                'last_update' => time(),
              ])
              ->execute();
          }
        }
        break;
    }
  }

  /**
   * Fetches an item from the 'novaposhta_lists' table.
   *
   * @param string $id
   *   The reference ID of the item.
   * @param string $type
   *   The type of the item.
   */
  public function listItem(string $id = '', string $type = '') {
    return \Drupal::database()->select('novaposhta_lists', 'l')
      ->fields('l')
      ->condition('l.type', $type)
      ->condition('l.ref_id', $id)
      ->execute()->fetchObject();
  }

  /**
   * Generates and retrieves the list of seat size options.
   *
   * This method combines the default seat options with the additional
   * configurable seat options obtained from the configuration settings.
   */
  public function getOptionsSeat() {
    $options = [];

    // Default:
    foreach ($this::DEF_OPTIONS_SEAT as $item) {
      $item = implode('x', $item);
      $options[$item] = $item;
    }

    // All config:
    foreach (\Drupal::config('novaposhta.OptionsSeat')->get('config.sizes') as $size) {
      $size = [
        $size['height'] ?? NULL,
        $size['width'] ?? NULL,
        $size['length'] ?? NULL,
      ];
      $size = array_filter($size);
      if (empty($size)) {
        continue;
      }
      $size = implode('x', $size);
      $options[$size] = $size;
    }
    return $options;
  }

}
