<?php

namespace Drupal\novaposhta;

use Drupal\Core\Url;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\novaposhta\Form\NovaPoshtaENForm;
use Drupal\novaposhta\Form\NovaPoshtaCreateUserForm;
use Drupal\novaposhta\Form\NovaPoshtaCreateContactPersonForm;
use Drupal\novaposhta\API\NovaPoshtaAPI;
use Drupal\basket\Admin\BasketDeleteConfirm;
use Drupal\novaposhta\Form\NovaPoshtaSettingsForm;

/**
 * Class of AdminPages.
 */
class AdminPages {

  /**
   * NovaPoshta object.
   *
   * @var \Drupal\novaposhta\NovaPoshta|object|null
   */
  protected $novaPoshta;

  /**
   * Basket object.
   *
   * @var \Drupal\basket\Basket|object|null
   */
  protected $basket;

  /**
   * BasketPopupManager object.
   *
   * @var \Drupal\basket\Plugins\Popup\BasketPopupManager|object|null
   */
  protected $basketPopup;

  /**
   * Constructs a new instance and initializes dependencies.
   */
  public function __construct() {
    $this->novaPoshta = \Drupal::getContainer()->get('NovaPoshta');
    $this->basket = \Drupal::getContainer()->get('Basket');
    $this->basketPopup = \Drupal::getContainer()->get('BasketPopup');
  }

  /**
   * Alters the provided element based on the given parameters.
   *
   * Modifies the structure and data of the element array depending on the
   * `page_type` and `page_subtype` specified in the $params argument.
   * Handles use cases for managing Nova Poshta-related data and forms,
   * such as creating invoices, viewing documents, creating contacts,
   * and updating document statuses.
   */
  public function alter(&$element, $params) {
    switch ($params['page_type']) {
      case'novaposhta':
        if (!empty(\Drupal::request()->query->get('create'))) {
          $element['#info']['content']['view'] = [
            '#type' => 'container',
            '#attributes' => [
              'class' => ['basket_table_wrap'],
            ], [
              '#type' => 'html_tag',
              '#tag' => 'div',
              '#attributes' => [
                'class' => ['b_title'],
              ],
              '#value' => $this->novaPoshta->trans('Create invoice'),
            ], [
              '#type' => 'container',
              '#attributes' => [
                'class' => ['b_content'],
              ],
              'form' => \Drupal::formBuilder()->getForm(new NovaPoshtaENForm(
                FALSE,
                NULL,
                \Drupal::request()->query->get('order')
              )),
            ],
          ];
        }
        elseif (!empty($num = \Drupal::request()->query->get('viewEn'))) {
          $novaPoshtaView = new NovaPoshtaView(NULL, $num);
          $element['#info']['content']['view'] = [
            'build' => $novaPoshtaView->build(),
            '#attached' => [
              'library' => ['novaposhta/js'],
              'drupalSettings' => [
                'npPostUpdate' => 1,
              ],
            ],
          ];
        }
        elseif (!empty($editID = \Drupal::request()->query->get('edit'))) {
          $en = $this->novaPoshta->loadEnById($editID);
          if (!empty($en->en_num)) {
            $element['#info']['content']['view'] = [
              '#type' => 'container',
              '#attributes' => [
                'class' => ['basket_table_wrap'],
              ], [
                '#type' => 'html_tag',
                '#tag' => 'div',
                '#value' => 'ЕН: ' . $en->en_num,
                '#attributes' => [
                  'class' => ['b_title'],
                ],
              ], [
                '#type' => 'container',
                '#attributes' => [
                  'class' => ['b_content'],
                ],
                'form' => \Drupal::formBuilder()
                  ->getForm(new NovaPoshtaENForm(FALSE, $en)),
              ],
            ];
          }
          else {
            $element['#info']['content']['view'] = $this->basket->getError(404);
          }
        }
        else {
          $element['#info']['content']['view'] = [
            'CreateLink' => [
              '#type' => 'link',
              '#title' => $this->novaPoshta->trans('Create invoice'),
              '#url' => new Url('basket.admin.pages', [
                'page_type' => 'novaposhta',
              ], [
                'query' => [
                  'create' => 'NEW',
                ],
                'attributes' => [
                  'id' => 'CreateLink',
                ],
              ]),
            ], [
              '#type' => 'container',
              '#attributes' => [
                'class' => ['basket_table_wrap'],
              ], [
                'title' => [
                  '#type' => 'container',
                  '#attributes' => [
                    'class' => ['b_title'],
                  ], [
                    '#markup' => $this->novaPoshta->trans('Invoices'),
                  ], [
                    '#type' => 'inline_template',
                    '#template' => '<a href="javascript:void(0);" class="button--link" onclick="{{ onclick }}" data-post="{{ post }}" title="{{ title }}"><span class="ico">{{ ico|raw }}</span> {{ text }}</a>',
                    '#context' => [
                      'text' => $this->novaPoshta->trans('Refresh list'),
                      'ico' => $this->basket->getIco('refresh.svg', 'novaposhta'),
                      'onclick' => 'basket_admin_ajax_link(this, \'' . Url::fromRoute('basket.admin.pages',
                        [
                          'page_type' => 'api-novaposhta-getDocumentList',
                        ])->toString() . '\')',
                      'post' => json_encode([
                        'dateStart' => date('d.m.Y',
                          strtotime('-' . NOVAPOSHTA_EN_DAYS . ' days')),
                        'dateStop' => date('d.m.Y'),
                      ]),
                    ],
                  ],
                ],
              ], [
                '#type' => 'container',
                '#attributes' => [
                  'class' => ['b_content b_content_novaposhta'],
                ],
                'views' => $this->basket->getView(NOVAPOSHTA_VIEW_ID,
                  NOVAPOSHTA_VIEW_DISPLAY_ID),
                '#attached' => [
                  'library' => ['novaposhta/admin'],
                ],
              ],
            ],
          ];

        }
        break;

      case'api':
        if ($params['page_subtype'] == 'novaposhta') {
          switch ($params['page_subtype1']) {
            case'createCounterparty':
              if (!empty($_POST['CounterpartyProperty'])) {
                $this->basketPopup->openModal(
                  $element,
                  $_POST['CounterpartyProperty'] == 'Recipient' ? $this->novaPoshta->t('Create recipient') : $this->novaPoshta->t('Create sender'),
                  \Drupal::formBuilder()->getForm(new NovaPoshtaCreateUserForm($_POST['CounterpartyProperty'])),
                  [
                    'width' => 400,
                    'class' => ['novaposhta_popup'],
                  ]
                );
              }
              break;

            case'createContactRecipient':
              if (!empty($_POST['CounterpartyRef'])) {
                $this->basketPopup->openModal(
                  $element,
                  $this->novaPoshta->t('Create a contact person'),
                  \Drupal::formBuilder()->getForm(new NovaPoshtaCreateContactPersonForm($_POST['CounterpartyRef'])),
                  [
                    'width' => 400,
                    'class' => ['novaposhta_popup'],
                  ]
                );
              }
              break;

            case'getDocumentList':
              if (!empty($_POST['dateStart']) && !empty($_POST['dateStop'])) {
                // Update invoice list:
                (new NovaPoshtaAPI())
                  ->updateDocumentList([
                    'GetFullList' => 1,
                    'DateTimeFrom' => $_POST['dateStart'],
                    'DateTimeTo' => $_POST['dateStop'],
                  ]);

                $element->addCommand(new InvokeCommand('body', 'append', ['<script>location.reload();</script>']));
              }
              break;

            case'viewInfo':
              if (!empty($_POST['viewID'])) {
                $novaPoshtaView = new NovaPoshtaView($_POST['viewID']);
                $this->basketPopup->openModal(
                  $element,
                  '',
                  $novaPoshtaView->build(),
                  [
                    'width' => 960,
                    'class' => ['novaposhta_popup'],
                  ]
                );
              }
              break;

            case'getStatusDocuments':
              if (!empty($_POST['np_EN'])) {
                $api = new NovaPoshtaAPI();
                $this->basketPopup->openModal(
                  $element,
                  '',
                  [
                    '#theme' => 'novaposhta_en_tracking',
                    '#attached' => [
                      'library' => ['novaposhta/admin'],
                    ],
                    '#info' => [
                      'en_num' => $_POST['np_EN'],
                      'request' => $api->getStatusDocuments([$_POST['np_EN']]),
                    ],
                  ], [
                    'width' => 400,
                    'class' => ['novaposhta_popup'],
                  ]
                );
              }
              break;

            case'deleteEN':
              if (!empty($_POST['npID'])) {
                $en = $this->novaPoshta->loadEnById($_POST['npID']);
                if (!empty($en->en_num)) {
                  if (!empty($_POST['confirm'])) {
                    $this->novaPoshta->deleteById($_POST['npID']);
                    $element->addCommand(new InvokeCommand('body', 'append', ['<script>location.reload();</script>']));
                  }
                  else {
                    $this->basketPopup->openModal(
                      $element,
                      $this->novaPoshta->trans('Delete')->__toString() . ': ' . $en->en_num,
                      BasketDeleteConfirm::confirmContent([
                        'onclick' => 'basket_admin_ajax_link(this, \'' . Url::fromRoute('basket.admin.pages', ['page_type' => 'api-novaposhta-deleteEN'])->toString() . '\')',
                        'post' => json_encode([
                          'npID' => $_POST['npID'],
                          'confirm' => 1,
                        ]),
                      ]), [
                        'width' => 400,
                        'class' => ['novaposhta_popup'],
                      ],
                    );
                  }
                }
              }
              break;

            case'updateInfo':
              if (!empty($_POST['enNum'])) {
                $api = new NovaPoshtaAPI();
                $api->getStatusDocuments([$_POST['enNum']]);
                $novaPoshtaView = new NovaPoshtaView(NULL, $_POST['enNum']);
                $element->addCommand(new ReplaceCommand('[data-en_num="' . $_POST['enNum'] . '"]', $novaPoshtaView->build()));
              }
              break;
          }
        }
        break;

      case'settings':
        if ($params['page_subtype'] == 'novaposhta') {
          $element['#info']['content']['view'] = [
            '#prefix' => '<div class="basket_table_wrap">',
            '#suffix' => '</div>',
            [
              '#prefix' => '<div class="b_title">',
              '#suffix' => '</div>',
              '#markup' => $this->novaPoshta->trans('Settings'),
            ], [
              '#prefix' => '<div class="b_content">',
              '#suffix' => '</div>',
              'form' => \Drupal::formBuilder()->getForm(new NovaPoshtaSettingsForm()),
              'template' => [
                '#type' => 'details',
                '#title' => $this->novaPoshta->trans('Template EN'),
                '#open' => FALSE,
                'form' => \Drupal::formBuilder()->getForm(new NovaPoshtaENForm(TRUE)),
              ],
            ],
          ];
        }
        break;
    }
  }

  /**
   * Builds and appends Nova Poshta EN links.
   *
   * @param array &$links
   *   The array of links to which the Nova Poshta link will be added.
   * @param object $order
   *   The order object for which the Nova Poshta EN link is generated.
   */
  public function orderLinks(array &$links, object $order) {
    if (\Drupal::currentUser()->hasPermission('access novaposhta en')) {
      $en = $this->loadOrderEn($order->id);
      if (empty($en->en_edit_id)) {
        $query = [
          'create' => 'NEW',
          'order' => $order->id,
        ];
      }
      else {
        $query = [
          'edit' => $en->en_edit_id,
        ];
      }
      $links['np_en'] = [
        '#type' => 'inline_template',
        '#template' => '<a href="{{ url }}" class="button--link" target="_blank"><span class="ico">{{ ico|raw }}</span> {{ text }}</a>',
        '#context' => [
          'text' => $this->novaPoshta->trans('EN'),
          'ico' => $this->basket->getIco('np.svg', 'novaposhta'),
          'target' => '_blank',
          'url' => Url::fromRoute('basket.admin.pages', [
            'page_type' => 'novaposhta',
          ], [
            'query' => $query,
          ])->toString(),
        ],
      ];
    }
  }

  /**
   * Loads an order from the "novaposhta_en_orders" database table by order ID.
   *
   * @param int $orderId
   *   The identifier of the order to load.
   */
  public function loadOrderEn(int $orderId) {
    $query = \Drupal::database()->select('novaposhta_en_orders', 'o');
    $query->condition('o.order_id', $orderId);
    $query->fields('o');
    $query->leftJoin('novaposhta_en', 'en', 'en.en_num = o.en_num');
    $query->addExpression('en.id', 'en_edit_id');
    return $query->execute()->fetchObject();
  }

}
