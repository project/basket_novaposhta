<?php

namespace Drupal\novaposhta;

use Drupal\novaposhta\API\NovaPoshtaAPI;

/**
 * Class of NovaPoshtaView.
 */
class NovaPoshtaView {

  /**
   * Basket object.
   *
   * @var \Drupal\basket\Basket|object|null
   */
  protected $basket;

  /**
   * NovaPoshta object.
   *
   * @var \Drupal\novaposhta\NovaPoshta|object|null
   */
  protected $novaPoshta;

  /**
   * NovaPoshtaAPI object.
   *
   * @var \Drupal\novaposhta\API\NovaPoshtaAPI
   */
  protected $api;

  /**
   * EN info.
   *
   * @var mixed
   */
  protected $en;

  /**
   * Constructor for the class.
   *
   * @param mixed|null $viewID
   *   The ID of the view, used to load specific data from the NovaPoshta API.
   * @param mixed|null $viewNUM
   *   The numeric identifier of the view.
   */
  public function __construct($viewID = NULL, $viewNUM = NULL) {
    $this->basket = \Drupal::getContainer()->get('Basket');
    $this->novaPoshta = \Drupal::getContainer()->get('NovaPoshta');
    $this->en = $this->novaPoshta->loadEnById($viewID);
    if (empty($this->en) && !empty($viewNUM)) {
      $this->en = $this->novaPoshta->loadEnByNum($viewNUM);
    }
    $this->api = new NovaPoshtaAPI();
    if (!empty($this->en->all_info)) {
      $this->en->all_info = unserialize($this->en->all_info);
      if (!empty($this->en->all_info['DateTime'])) {
        $this->en->all_info['DateTime'] = strtotime($this->en->all_info['DateTime']);
      }
      if (!empty($this->en->all_info['PayerType'])) {
        $payerTypes = $this->api->getTypesOfPayers([]);
        if (!empty($payerTypes[$this->en->all_info['PayerType']])) {
          $this->en->all_info['PayerType'] = $payerTypes[$this->en->all_info['PayerType']];
        }
      }
      if (!empty($this->en->all_info['PaymentMethod'])) {
        $paymentMethods = $this->api->getPaymentForms([]);
        if (!empty($paymentMethods[$this->en->all_info['PaymentMethod']])) {
          $this->en->all_info['PaymentMethod'] = $paymentMethods[$this->en->all_info['PaymentMethod']];
        }
      }
      if (!empty($this->en->all_info['CargoType'])) {
        $cargoTypes = $this->api->getCargoTypes([]);
        if (!empty($cargoTypes[$this->en->all_info['CargoType']])) {
          $this->en->all_info['CargoType'] = $cargoTypes[$this->en->all_info['CargoType']];
        }
      }
      if (!empty($this->en->all_info['Recipient'])) {
        $recipient = $this->en->all_info['Recipient'];
        $recipients = $this->api->getCounterparties(['CounterpartyProperty' => 'Recipient']);
        if (!empty($recipients[$this->en->all_info['Recipient']])) {
          $this->en->all_info['Recipient'] = $recipients[$this->en->all_info['Recipient']];
        }
        if (!empty($this->en->all_info['ContactRecipient'])) {
          $contactRecipient = $this->api->getCounterpartyContactPersons(['Ref' => $recipient], FALSE, $this->en->all_info['RecipientsPhone']);
          if (!empty($contactRecipient['data'][0])) {
            $this->en->all_info['ContactRecipient'] = $contactRecipient['data'][0]['LastName'] . ' ' . $contactRecipient['data'][0]['FirstName'];
          }
        }
      }
      elseif (!empty($this->en->all_info['RecipientName'])) {
        $this->en->all_info['ContactRecipient'] = $this->en->all_info['RecipientName'];
      }
      if (!empty($this->en->all_info['RecipientContactPerson'])) {
        $this->en->all_info['ContactRecipient'] = $this->en->all_info['RecipientContactPerson'];
      }
      if (empty($this->en->all_info['CityRecipientDescription'])) {
        $this->en->all_info['CityRecipientDescription'] = implode(', ', array_filter([
          $this->en->all_info['RecipientCityName'] ?? NULL,
          $this->en->all_info['RecipientAddressName'] ?? NULL,
          $this->en->all_info['RecipientHouse'] ?? NULL,
          $this->en->all_info['RecipientFlat'] ?? NULL,
        ]));
      }
    }
  }

  /**
   * Builds and prepares the data structure.
   */
  public function build() {
    if (empty($this->en)) {
      return $this->basket->getError(404);
    }
    $this->en->print_link = $this->api->getPrintLink($this->en->en_num);
    return [
      '#theme' => 'novaposhta_en_view',
      '#info' => $this->en,
      '#attached' => [
        'library' => ['novaposhta/admin'],
      ],
    ];
  }

  /**
   * Retrieves a printable link for the provided entity if available.
   */
  public function getPrintLink() {
    if (empty($this->en)) {
      return [];
    }
    if (empty($this->en->en_num)) {
      return [];
    }
    $getPrintLink = $this->api->getPrintLink($this->en->en_num);
    if (!empty($getPrintLink)) {
      return [
        'text' => $this->novaPoshta->trans('Print'),
        'ico' => $this->basket->getIco('print.svg', 'novaposhta'),
        'url' => $getPrintLink,
        'target' => '_blank',
      ];
    }
  }

}
