<?php

namespace Drupal\novaposhta\API;

/**
 * Nova Poshta API Class.
 *
 * @author lis-dev
 * @see https://my.novaposhta.ua/data/API2-200215-1622-28.pdf
 * @see https://github.com/lis-dev
 * @license MIT
 */
class NovaPoshtaApi2 {

  /**
   * Key for API NovaPoshta.
   *
   * @var string|null
   * @see https://my.novaposhta.ua/settings/index#apikeys
   */
  protected ?string $key;

  /**
   * Throw exceptions when in response is error.
   *
   * @var bool
   */
  protected mixed $throwErrors = FALSE;

  /**
   * Format of returned data - array, json, xml.
   *
   * @var string
   */
  protected string $format = 'array';

  /**
   * Language of response.
   *
   * @var string
   */
  protected string $language = 'ru';

  /**
   * Connection type (curl | file_get_contents).
   *
   * @var string
   */
  protected string $connectionType = 'curl';

  /**
   * Areas list.
   *
   * @var array
   */
  protected array $areas;

  /**
   * Set current model for methods save(), update(), delete().
   *
   * @var string
   */
  protected string $model = 'Common';

  /**
   * Set method of current model.
   *
   * @var string|null
   */
  protected ?string $method;

  /**
   * Set params of current method of current model.
   *
   * @var array|null
   */
  protected ?array $params;

  /**
   * Default constructor.
   */
  public function __construct($key, $language = 'ru', $throwErrors = FALSE, $connectionType = 'curl') {
    $this->throwErrors = $throwErrors;
    return $this
      ->setKey($key)
      ->setLanguage($language)
      ->setConnectionType($connectionType)
      ->model('Common');
  }

  /**
   * Setter for key property.
   */
  public function setKey($key) {
    $this->key = $key;
    return $this;
  }

  /**
   * Getter for key property.
   */
  public function getKey() {
    return $this->key;
  }

  /**
   * Setter for $connectionType property.
   */
  public function setConnectionType($connectionType) {
    $this->connectionType = $connectionType;
    return $this;
  }

  /**
   * Getter for $connectionType property.
   */
  public function getConnectionType() {
    return $this->connectionType;
  }

  /**
   * Setter for language property.
   */
  public function setLanguage($language) {
    $this->language = $language;
    return $this;
  }

  /**
   * Getter for language property.
   */
  public function getLanguage() {
    return $this->language;
  }

  /**
   * Setter for format property.
   */
  public function setFormat($format) {
    $this->format = $format;
    return $this;
  }

  /**
   * Getter for format property.
   */
  public function getFormat() {
    return $this->format;
  }

  /**
   * Prepares the given data based on the specified format.
   */
  private function prepare($data) {

    // Returns array:
    if ($this->format == 'array') {
      $result = is_array($data)
        ? $data
        : json_decode($data, 1);

      // If error exists, throw Exception:
      if ($this->throwErrors && !empty($result['errors'])) {
        $errors = is_array($result['errors']) ? implode("\n", $result['errors']) : $result['errors'];
        throw new \Exception($errors);
      }

      return $result;
    }

    // Returns json or xml document:
    return $data;
  }

  /**
   * Converts an associative array to an XML string representation.
   */
  private function array2xml(array $array, $xml = FALSE) {
    if ($xml === FALSE) {
      $xml = new \SimpleXMLElement('<root/>');
    }
    foreach ($array as $key => $value) {
      if (is_array($value)) {
        $this->array2xml($value, $xml->addChild($key));
      }
      else {
        $xml->addChild($key, $value);
      }
    }
    return $xml->asXML();
  }

  /**
   * Sends a request to the specified model and method with optional parameters.
   */
  private function request($model, $method, $params = NULL) {

    // Get required URL:
    $url = $this->format === 'xml'
      ? 'https://api.novaposhta.ua/v2.0/xml/'
      : 'https://api.novaposhta.ua/v2.0/json/';

    $data = [
      'apiKey' => $this->key,
      'modelName' => $model,
      'calledMethod' => $method,
      'language' => $this->language,
      'methodProperties' => !empty($params) ? $params : new \stdClass(),
    ];

    // Convert data to neccessary format:
    $post = $this->format === 'xml'
      ? $this->array2xml($data)
      : json_encode($data, JSON_THROW_ON_ERROR);

    if ($this->getConnectionType() === 'curl') {
      $ch = curl_init($url);

      curl_setopt_array($ch, [
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => ['Content-Type: ' . ($this->format === 'xml' ? 'text/xml' : 'application/json')],
        CURLOPT_HEADER => FALSE,
        CURLOPT_POST => TRUE,
        CURLOPT_SSL_VERIFYPEER => FALSE,
        CURLOPT_POSTFIELDS => $post,
      ]);

      $result = curl_exec($ch);

      if ($result === FALSE) {
        throw new \RuntimeException('cURL Error: ' . curl_error($ch));
      }

      curl_close($ch);
    }
    else {
      $context = stream_context_create([
        'http' => [
          'method' => 'POST',
          'header' => "Content-type: application/x-www-form-urlencoded\r\n",
          'content' => $post,
        ],
      ]);

      $result = file_get_contents($url, FALSE, $context);

      if ($result === FALSE) {
        throw new \RuntimeException('Error sending request through file_get_contents');
      }
    }

    return $this->prepare($result);
  }

  /**
   * Sets or retrieves the current model.
   */
  public function model($model = '') {
    if (empty($model)) {
      return $this->model;
    }

    $this->model = $model;
    $this->method = NULL;
    $this->params = NULL;
    return $this;
  }

  /**
   * Sets or retrieves the method value.
   */
  public function method($method = '') {
    if (empty($method)) {
      return $this->method;
    }

    $this->method = $method;
    $this->params = NULL;

    return $this;
  }

  /**
   * Sets the parameters for the current instance.
   */
  public function params($params) {
    $this->params = $params;
    return $this;
  }

  /**
   * Executes a request using the specified model, method, and parameters.
   */
  public function execute() {
    return $this->request($this->model, $this->method, $this->params);
  }

  /**
   * Tracks the status of documents based on the provided tracking information.
   */
  public function documentsTracking($track) {
    return $this->request('InternetDocument', 'documentsTracking', ['Documents' => ['item' => $track]]);
  }

  /**
   * Retrieves a list of cities based on the provided parameters.
   */
  public function getCities($page = 0, $findByString = '', $ref = '') {
    return $this->request('Address', 'getCities', [
      'Page' => $page,
      'FindByString' => $findByString,
      'Ref' => $ref,
    ]);
  }

  /**
   * Retrieves a list of warehouses in the specified city.
   */
  public function getWarehouses($cityRef, $page = 0) {
    return $this->request('Address', 'getWarehouses', [
      'CityRef' => $cityRef,
      'Page' => $page,
    ]);
  }

  /**
   * Finds the nearest warehouse based on the provided search strings.
   */
  public function findNearestWarehouse($searchStringArray) {
    $searchStringArray = (array) $searchStringArray;
    return $this->request('Address', 'findNearestWarehouse', [
      'SearchStringArray' => $searchStringArray,
    ]);
  }

  /**
   * Retrieves a warehouse from a specified city.
   */
  public function getWarehouse($cityRef, $description = '') {
    $warehouses = $this->getWarehouses($cityRef);
    $data = [];
    $error = NULL;

    if (isset($warehouses['data']) && is_array($warehouses['data'])) {
      $warehousesCount = count($warehouses['data']);

      if ($warehousesCount === 1) {
        $data = $warehouses['data'][0];
      }
      elseif ($warehousesCount > 1 && !empty($description)) {
        foreach ($warehouses['data'] as $warehouse) {
          if (
            mb_stripos($warehouse['Description'], $description) !== FALSE ||
            mb_stripos($warehouse['DescriptionRu'], $description) !== FALSE
          ) {
            $data = $warehouse;
            break;
          }
        }
      }
    }

    if (empty($data)) {
      $error = 'Warehouse was not found';
    }

    // Return data in same format like NovaPoshta API:
    return $this->prepare([
      'success' => is_null($error),
      'data' => $data ? [$data] : [],
      'errors' => $error ? [$error] : [],
      'warnings' => [],
      'info' => [],
    ]);
  }

  /**
   * Retrieves a list of streets based on the specified city reference.
   */
  public function getStreet($cityRef, $findByString = '', $page = 0) {
    return $this->request('Address', 'getStreet', [
      'FindByString' => $findByString,
      'CityRef' => $cityRef,
      'Page' => $page,
    ]);
  }

  /**
   * Searches for areas in the provided array that match.
   */
  protected function findArea(array $areas, $findByString = '', $ref = '') {
    $data = [];
    if (empty($findByString) && empty($ref)) {
      return $data;
    }
    foreach ($areas as $key => $area) {
      $found = $findByString
        ? (
          mb_stripos($area['Description'], $findByString) !== FALSE ||
          mb_stripos($area['DescriptionRu'], $findByString) !== FALSE ||
          mb_stripos($area['Area'], $findByString) !== FALSE ||
          mb_stripos($area['AreaRu'], $findByString) !== FALSE
        )
        : ($key === $ref);
      if ($found) {
        $area['Ref'] = $key;
        $data[] = $area;
        break;
      }
    }
    return $data;
  }

  /**
   * Retrieves area information based on the provided string or reference.
   */
  public function getArea($findByString = '', $ref = '') {

    // Load areas list from file:
    if (empty($this->areas)) {
      $this->areas = include dirname(__FILE__) . '/NovaPoshtaApi2Areas.php';
    }

    $data = $this->findArea($this->areas, $findByString, $ref);
    $error = NULL;

    // Error:
    if (empty($data)) {
      $error = 'Area was not found';
    }

    // Return data in same format like NovaPoshta API:
    return $this->prepare([
      'success' => is_null($error),
      'data' => $data ?? [],
      'errors' => $error ? [$error] : [],
      'warnings' => [],
      'info' => [],
    ]);
  }

  /**
   * Retrieves a list of areas based on the provided reference and page number.
   */
  public function getAreas($ref = '', $page = 0) {
    return $this->request('Address', 'getAreas', [
      'Ref' => $ref,
      'Page' => $page,
    ]);
  }

  /**
   * Filters and retrieves a list of cities belonging to the specified region.
   */
  protected function findCityByRegion($cities, $areaName) {
    $data = [];

    // Get region id:
    $area = $this->getArea($areaName);

    if (!empty($area['success']) && !empty($area['data'][0]['Ref'])) {
      $areaRef = $area['data'][0]['Ref'];

      if (!empty($cities['data']) && is_array($cities['data'])) {
        foreach ($cities['data'] as $city) {
          if ($city['Area'] === $areaRef) {
            $data[] = $city;
          }
        }
      }
    }
    return $data;
  }

  /**
   * Retrieves a city's information based on the provided.
   */
  public function getCity($cityName, $areaName = '') {

    // Get cities by name:
    $cities = $this->getCities(0, $cityName);
    $data = NULL;
    $error = NULL;

    if (is_array($cities['data']) && !empty($cities['data'])) {

      // If cities more then one, calculate current by area name:
      $data = (count($cities['data']) > 1)
        ? $this->findCityByRegion($cities, $areaName)
        : $cities['data'][0];
    }

    // Error:
    if (empty($data)) {
      $error = 'City was not found';
    }

    // Return data in same format like NovaPoshta API:
    return $this->prepare([
      'success' => is_null($error),
      'data' => $data ? [$data] : [],
      'errors' => $error ? [$error] : [],
      'warnings' => [],
      'info' => [],
    ]);
  }

  /**
   * Dynamically handles method calls mapped to the Common model.
   */
  public function __call($method, $arguments) {
    $common_model_method = [
      'getTypesOfCounterparties',
      'getBackwardDeliveryCargoTypes',
      'getCargoDescriptionList',
      'getCargoTypes',
      'getDocumentStatuses',
      'getOwnershipFormsList',
      'getPalletsList',
      'getPaymentForms',
      'getTimeIntervals',
      'getServiceTypes',
      'getTiresWheelsList',
      'getTraysList',
      'getTypesOfAlternativePayers',
      'getTypesOfPayers',
      'getTypesOfPayersForRedelivery',
      'getPalletsList',
    ];

    // Call method of Common model:
    if (in_array($method, $common_model_method)) {
      return $this
        ->model('Common')
        ->method($method)
        ->params(NULL)
        ->execute();
    }
  }

  /**
   * Deletes an existing record based on the provided parameters.
   */
  public function delete($params) {
    return $this->request($this->model, 'delete', $params);
  }

  /**
   * Updates an existing record in the system with the specified parameters.
   */
  public function update($params) {
    return $this->request($this->model, 'update', $params);
  }

  /**
   * Saves a new record or updates an existing one for the specified model.
   */
  public function save($params) {
    return $this->request($this->model, 'save', $params);
  }

  /**
   * Retrieves a list of counterparties based on the provided parameters.
   */
  public function getCounterparties($counterpartyProperty = 'Recipient', $page = NULL, $findByString = NULL, $cityRef = NULL) {

    // Any param can be skipped:
    $params = [
      'CounterpartyProperty' => $counterpartyProperty ?: 'Recipient',
    ];

    if (!is_null($page)) {
      $params['Page'] = $page;
    }
    if (!is_null($findByString)) {
      $params['FindByString'] = $findByString;
    }
    if (!is_null($cityRef)) {
      $params['CityRef'] = $cityRef;
    }
    return $this->request('Counterparty', 'getCounterparties', $params);
  }

  /**
   * Clones a loyalty counterparty sender for the specified city reference.
   */
  public function cloneLoyaltyCounterpartySender($cityRef) {
    return $this->request('Counterparty', 'cloneLoyaltyCounterpartySender', ['CityRef' => $cityRef]);
  }

  /**
   * Retrieves a list of contact persons associated with a counterparty.
   */
  public function getCounterpartyContactPersons($ref, $page = 0, $name = '') {
    return $this->request(
      'Counterparty',
      'getCounterpartyContactPersons',
      ['Ref' => $ref, 'Page' => $page, 'FindByString' => $name]
    );
  }

  /**
   * Retrieves the addresses associated with a specific counterparty reference.
   */
  public function getCounterpartyAddresses($ref, $page = 0) {
    return $this->request(
      'Counterparty',
      'getCounterpartyAddresses',
      ['Ref' => $ref, 'Page' => $page]
    );
  }

  /**
   * Retrieves the options available for a specific counterparty.
   */
  public function getCounterpartyOptions($ref) {
    return $this->request('Counterparty', 'getCounterpartyOptions', ['Ref' => $ref]);
  }

  /**
   * Retrieves counterparty information based.
   */
  public function getCounterpartyByEdrpou($edrpou, $cityRef) {
    return $this->request(
      'Counterparty',
      'getCounterpartyByEDRPOU',
      ['EDRPOU' => $edrpou, 'cityRef' => $cityRef]
    );
  }

  /**
   * Retrieves the price of a document based on the provided parameters.
   */
  public function getDocumentPrice($citySender, $cityRecipient, $serviceType, $weight, $cost) {
    return $this->request('InternetDocument', 'getDocumentPrice', [
      'CitySender' => $citySender,
      'CityRecipient' => $cityRecipient,
      'ServiceType' => $serviceType,
      'Weight' => $weight,
      'Cost' => $cost,
    ]);
  }

  /**
   * Retrieves the estimated delivery date for a document.
   */
  public function getDocumentDeliveryDate($citySender, $cityRecipient, $serviceType, $dateTime) {
    return $this->request('InternetDocument', 'getDocumentDeliveryDate', [
      'CitySender' => $citySender,
      'CityRecipient' => $cityRecipient,
      'ServiceType' => $serviceType,
      'DateTime' => $dateTime,
    ]);
  }

  /**
   * Retrieves a list of documents based on the provided parameters.
   */
  public function getDocumentList($params = NULL) {
    return $this->request('InternetDocument', 'getDocumentList', $params ?: NULL);
  }

  /**
   * Retrieves the details of a specific internet document.
   */
  public function getDocument($ref) {
    return $this->request('InternetDocument', 'getDocument', [
      'Ref' => $ref,
    ]);
  }

  /**
   * Generates a report based on the provided parameters.
   */
  public function generateReport($params) {
    return $this->request('InternetDocument', 'generateReport', $params);
  }

  /**
   * Validates and updates the recipient information for an internet document.
   */
  protected function checkInternetDocumentRecipient(array &$counterparty): void {

    // List of required fields:
    $requiredFields = ['FirstName', 'LastName', 'Phone'];

    // Check required fields:
    foreach ($requiredFields as $field) {
      if (empty($counterparty[$field])) {
        throw new \InvalidArgumentException("$field is a required field for recipient");
      }
    }

    // Checking dependent fields (City or CityRef):
    if (empty($counterparty['City']) && empty($counterparty['CityRef'])) {
      throw new \InvalidArgumentException("City or CityRef is a required field for recipient");
    }

    // Checking dependent fields (Region or CityRef):
    if (empty($counterparty['Region']) && empty($counterparty['CityRef'])) {
      throw new \InvalidArgumentException("Region or CityRef is a required field for recipient");
    }

    // Setting default values:
    $counterparty['CounterpartyType'] ??= 'PrivatePerson';
  }

  /**
   * Validates and sets default values for the parameters.
   */
  protected function checkInternetDocumentParams(array &$params): void {
    $requiredFields = ['Description', 'Weight', 'Cost'];
    foreach ($requiredFields as $field) {
      if (empty($params[$field])) {
        throw new \InvalidArgumentException("$field is required filed for new Internet document");
      }
    }

    if (!isset($params['DateTime'])) {
      $params['DateTime'] = date('d.m.Y');
    }
    if (!isset($params['ServiceType'])) {
      $params['ServiceType'] = 'WarehouseWarehouse';
    }
    if (!isset($params['PaymentMethod'])) {
      $params['PaymentMethod'] = 'Cash';
    }
    if (!isset($params['PayerType'])) {
      $params['PayerType'] = 'Recipient';
    }
    if (!isset($params['SeatsAmount'])) {
      $params['SeatsAmount'] = '1';
    }
    if (!isset($params['CargoType'])) {
      $params['CargoType'] = 'Cargo';
    }
    if (!isset($params['VolumeGeneral'])) {
      $params['VolumeGeneral'] = '0.0004';
    }
  }

  /**
   * Creates a new internet document using the provided sender.
   */
  public function newInternetDocument($sender, $recipient, $params) {

    // Check for required params and set defaults:
    $this->checkInternetDocumentRecipient($recipient);
    $this->checkInternetDocumentParams($params);
    if (!$sender['CitySender']) {
      $senderCity = $this->getCity($sender['City'], $sender['Region']);
      $sender['CitySender'] = $senderCity['data'][0]['Ref'];
    }
    $sender['CityRef'] = $sender['CitySender'];
    if (!$sender['SenderAddress'] && $sender['CitySender'] && $sender['Warehouse']) {
      $senderWarehouse = $this->getWarehouse($sender['CitySender'], $sender['Warehouse']);
      $sender['SenderAddress'] = $senderWarehouse['data'][0]['Ref'];
    }
    if (!$sender['Sender']) {
      $sender['CounterpartyProperty'] = 'Sender';

      // Set full name to Description if is not set:
      if (!$sender['Description']) {
        $sender['Description'] = $sender['LastName'] . ' ' . $sender['FirstName'] . ' ' . $sender['MiddleName'];
      }

      // Check for existing sender:
      $senderCounterpartyExisting = $this->getCounterparties('Sender', 1, $sender['Description'], $sender['CityRef']);

      // Copy user to the selected city if user doesn't exists there:
      if ($senderCounterpartyExisting['data'][0]['Ref']) {

        // Counterparty exists:
        $sender['Sender'] = $senderCounterpartyExisting['data'][0]['Ref'];
        $contactSender = $this->getCounterpartyContactPersons($sender['Sender']);
        $sender['ContactSender'] = $contactSender['data'][0]['Ref'];
        $sender['SendersPhone'] = $sender['Phone'] ?: $contactSender['data'][0]['Phones'];
      }
    }

    // Prepare recipient data:
    $recipient['CounterpartyProperty'] = 'Recipient';
    $recipient['RecipientsPhone'] = $recipient['Phone'];
    if (!$recipient['CityRecipient']) {
      $recipientCity = $this->getCity($recipient['City'], $recipient['Region']);
      $recipient['CityRecipient'] = $recipientCity['data'][0]['Ref'];
    }
    $recipient['CityRef'] = $recipient['CityRecipient'];
    if (!$recipient['RecipientAddress']) {
      $recipientWarehouse = $this->getWarehouse($recipient['CityRecipient'], $recipient['Warehouse']);
      $recipient['RecipientAddress'] = $recipientWarehouse['data'][0]['Ref'];
    }
    if (!$recipient['Recipient']) {
      $recipientCounterparty = $this->model('Counterparty')->save($recipient);
      $recipient['Recipient'] = $recipientCounterparty['data'][0]['Ref'];
      $recipient['ContactRecipient'] = $recipientCounterparty['data'][0]['ContactPerson']['data'][0]['Ref'];
    }

    // Full params is merge of arrays $sender, $recipient, $params:
    $paramsInternetDocument = array_merge($sender, $recipient, $params);

    // Creating new Internet Document:
    return $this->model('InternetDocument')->save($paramsInternetDocument);
  }

  /**
   * Generates a formatted link for accessing specific orders.
   */
  protected function printGetLink($method, $documentRefs, $type) {
    $data = 'https://my.novaposhta.ua/orders/' . $method . '/orders[]/' . implode(',', $documentRefs)
      . '/type/' . str_replace('_link', '', $type)
      . '/apiKey/' . $this->key;
    // Return data in same format like NovaPoshta API:
    return $this->prepare([
      'success' => TRUE,
      'data' => [$data],
      'errors' => [],
      'warnings' => [],
      'info' => [],
    ]);
  }

  /**
   * Generates a print output or link for the specified documents.
   */
  public function printDocument($documentRefs, $type = 'html') {
    $documentRefs = (array) $documentRefs;

    // If needs link:
    if ($type == 'html_link' || $type == 'pdf_link') {
      return $this->printGetLink('printDocument', $documentRefs, $type);
    }

    // If needs data:
    return $this->request(
      'InternetDocument',
      'printDocument',
      ['DocumentRefs' => $documentRefs, 'Type' => $type]
    );
  }

  /**
   * Generates and retrieves markings for specified documents.
   */
  public function printMarkings($documentRefs, $type = 'new_html') {
    $documentRefs = (array) $documentRefs;

    // If needs link:
    if ($type == 'html_link' || $type == 'pdf_link') {
      return $this->printGetLink('printMarkings', $documentRefs, $type);
    }

    // If needs data:
    return $this->request(
      'InternetDocument',
      'printMarkings',
      ['DocumentRefs' => $documentRefs, 'Type' => $type]
    );
  }

  /**
   * Saves a counterparty using the provided parameters.
   */
  public function saveCounterparty($params) {
    return $this->request('Counterparty', 'save', $params);
  }

  /**
   * Saves a contact person with the provided parameters.
   */
  public function saveContactPerson($params) {
    return $this->request('ContactPerson', 'save', $params);
  }

  /**
   * Saves a new internet document with the provided parameters.
   */
  public function saveInternetDocumentNew($params) {
    return $this->request('InternetDocument', 'save', $params);
  }

  /**
   * Retrieves the tracking status of a document using the provided parameters.
   */
  public function getTrackingDocument($params) {
    return $this->request('TrackingDocument', 'getStatusDocuments', $params);
  }

  /**
   * Deletes an existing internet document using the provided parameters.
   */
  public function internetDocumentDelete($params) {
    return $this->request('InternetDocument', 'delete', $params);
  }

  /**
   * Updates an existing internet document with the provided parameters.
   */
  public function internetDocumentUpdate($params) {
    return $this->request('InternetDocument', 'update', $params);
  }

  /**
   * Searches for settlements based on the provided parameters.
   */
  public function searchSettlements($params) {
    return $this->request('Address', 'searchSettlements', $params);
  }

  /**
   * Searches for streets within a settlement based on the provided parameters.
   */
  public function searchSettlementStreets($params) {
    return $this->request('Address', 'searchSettlementStreets', $params);
  }

  /**
   * Retrieves the message code text by invoking.
   */
  public function getMessageCodeText() {
    return $this->request('CommonGeneral', 'getMessageCodeText', []);
  }

}
