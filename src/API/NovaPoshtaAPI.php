<?php

namespace Drupal\novaposhta\API;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;

/**
 * Class of NovaPoshtaAPI.
 */
class NovaPoshtaAPI {

  use DependencySerializationTrait;

  /**
   * API key.
   *
   * @var array|mixed|string|null
   */
  public $apiKEY;

  /**
   * API key default.
   *
   * @var array|mixed|null
   */
  public $apiKEYDef;

  /**
   * NovaPoshta object.
   *
   * @var \Drupal\novaposhta\NovaPoshta|object|null
   */
  protected $novaPoshta;

  /**
   * NovaPoshtaApi2 object.
   *
   * @var \Drupal\novaposhta\API\NovaPoshtaApi2
   */
  protected $api;

  /**
   * NovaPoshtaApi2 object.
   *
   * @var \Drupal\novaposhta\API\NovaPoshtaApi2
   */
  protected $api2;

  /**
   * Dir path.
   *
   * @var string
   */
  protected $dir;

  /**
   * Is Cache?
   *
   * @var bool
   */
  protected $notCache = FALSE;

  /**
   * Language ID.
   *
   * @var string
   */
  protected $langcode;

  /**
   * Database object.
   *
   * @var \Drupal\Core\Database\Connection|object|null
   */
  protected $db;

  /**
   * Constructs a new instance of the class.
   *
   * @param string|null $apiKEY
   *   The API key for Nova Poshta.
   * @param bool $notCache
   *   Indicates whether to disable caching. Defaults to FALSE.
   */
  public function __construct($apiKEY = NULL, $notCache = FALSE) {
    $this->langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->apiKEYDef = \Drupal::config('novaposhta.settings')->get('config.api_key');
    $this->notCache = $notCache;
    if (empty($apiKEY)) {
      $apiKEY = $this->apiKEYDef;
    }
    $this->apiKEY = $apiKEY;
    $this->novaPoshta = \Drupal::getContainer()->get('NovaPoshta');
    $this->api = new NovaPoshtaApi2($this->apiKEY, $this->langcode);
    $this->api2 = new NovaPoshtaApi2(NULL, $this->langcode);
    $this->dir = $this->novaPoshta::NOVAPOCHTA_DIR . '/' . date('W_m_Y');
    $this->db = \Drupal::getContainer()->get('database');
  }

  /**
   * List of areas.
   */
  public function getRegions() {
    $query = \Drupal::database()->select('novaposhta_lists', 'l');
    $query->fields('l', ['ref_id', 'name']);
    $query->condition('l.type', 'area');
    $query->innerJoin('novaposhta_lists', 'pl', 'pl.parent_id = l.ref_id');
    $options = $query->execute()->fetchAllKeyed();
    return $options ?? [];
  }

  /**
   * List of cities.
   */
  public function getCitis($params = []) {
    $query = \Drupal::database()->select('novaposhta_lists', 'l');
    $query->fields('l', ['ref_id', 'name']);
    $query->condition('l.type', 'city');
    $query->condition('l.parent_id', $params['region']);
    $options = $query->execute()->fetchAllKeyed();
    return $options ?? [];
  }

  /**
   * Retrieves city information based on the provided parameters.
   *
   * @param array $params
   *   An associative array of parameters where 'Ref' is required.
   *   If 'Ref' is empty, the method will return an empty array.
   */
  public function getCity(array $params) {
    if (empty($params['Ref'])) {
      return [];
    }
    $options = $this->getCache($params, 'getCity');
    if (!empty($options)) {
      return $options;
    }
    $options = [];
    $results = $this->api2->getCities(0, '', $params['Ref']);
    if (!empty($results['data'][0])) {
      $options = $results['data'][0];
    }
    $this->setCache($options, $params, 'getCity');
    return $options;
  }

  /**
   * List of branches.
   */
  public function getPoints($params) {
    if (empty($params['city'])) {
      return [];
    }
    $options = $this->getCache($params, 'point');
    if (!empty($options)) {
      return $options;
    }
    $options = [];
    $results = $this->api2->getWarehouses($params['city']);
    if (!empty($results['data'])) {
      foreach ($results['data'] as $row) {
        $options[$row['Ref']] = str_replace('№', '№ ', $row['Description']);
      }
    }
    $this->setCache($options, $params, 'point');
    return $options;
  }

  /**
   * Getting shipping costs.
   */
  public function getCost($senderCity, $recipientCity, $weight = 1, $price = 0) {
    return $this->api->getDocumentPrice($senderCity, $recipientCity, 'WarehouseDoors', $weight, $price);
  }

  /**
   * Tracking.
   */
  public function getTrackingDocument($params) {
    return $this->api->getTrackingDocument($params);
  }

  /**
   * Download the list of Counterparties of senders/recipients/third parties.
   */
  public function getCounterparties($params, $resetCache = FALSE) {
    if (empty($params['CounterpartyProperty'])) {
      return [];
    }
    if (!$this->notCache) {
      $options = $this->getCache($params, 'getCounterparties');
      if (!empty($options) && !$resetCache) {
        return $options;
      }
    }
    $options = [];
    $results = $this->api->getCounterparties($params['CounterpartyProperty'], 1);
    if (!empty($results['data'][0]['Description'])) {
      foreach ($results['data'] as $key => $value) {
        $options[$value['Ref']] = !empty($value['OwnershipFormDescription']) ? $value['OwnershipFormDescription'] : $value['Description'];
        if ($value['CounterpartyType'] == 'Organization') {
          $options[$value['Ref']] .= ' "' . $value['Description'] . '"';
        }
      }
    }
    if (!$this->notCache) {
      $this->setCache($options, $params, 'getCounterparties');
    }
    return $options;
  }

  /**
   * Download the list of contact persons of the Counterparty.
   */
  public function getCounterpartyContactPersons($params, $resetCache = FALSE, $name = NULL) {
    if (empty($params['Ref'])) {
      return [];
    }
    if ($name) {
      return $this->api->getCounterpartyContactPersons($params['Ref'], 0, $name);
    }
    $options = $this->getCache($params, 'getCounterpartyContactPersons');
    if (!empty($options) && !$resetCache) {
      return $options;
    }
    $options = [];
    $resultData = $this->getCache(['Ref' => $params['Ref']], 'getContactPersonsAll');
    if (empty($resultData) || !empty($resetCache)) {
      $resultData = [];
      $page = 0;
      $results = $this->api->getCounterpartyContactPersons($params['Ref'], $page);
      if (!empty($results['data'])) {
        foreach ($results['data'] as $dataRow) {
          $resultData[$dataRow['Ref']] = $dataRow;
        }
        if (!empty($results['info']['totalCount']) && $results['info']['totalCount'] > count($resultData)) {
          $page++;
          $this->getContactPersonsAlters($resultData, $results['info']['totalCount'], $params['Ref'], $page);
        }
      }
      $this->setCache($resultData, ['Ref' => $params['Ref']], 'getContactPersonsAll');
    }
    if (!empty($resultData)) {
      if (!empty($params['Contact'])) {
        foreach ($resultData as $row) {
          if ($params['Contact'] == $row['Ref'] && !empty($row['Phones'])) {
            $options[$row['Phones']] = $row['Phones'];
          }
        }
        $options['PhoneCustom'] = $this->novaPoshta->t('Add another phone number');
      }
      else {
        foreach ($resultData as $row) {
          $name = !empty($row['Description']) ? [$row['Description']] : [];
          if (empty($name)) {
            if (empty($row['LastName'])) {
              $name[] = $row['LastName'];
            }
            if (empty($row['FirstName'])) {
              $name[] = $row['FirstName'];
            }
            if (empty($row['MiddleName'])) {
              $name[] = $row['MiddleName'];
            }
          }
          $options[$row['Ref']] = implode(' ', $name);
        }
      }
    }
    $this->setCache($options, $params, 'getCounterpartyContactPersons');
    return $options;
  }

  /**
   * Retrieves and appends contact persons' details.
   *
   * @param array &$resultData
   *   Reference to the array where contact persons' data will be stored.
   * @param int $max
   *   The maximum number of contact persons to retrieve.
   * @param string $ref
   *   The reference identifier for the counterparty.
   * @param int $page
   *   The current page number for paginated API requests.
   */
  private function getContactPersonsAlters(array &$resultData, int $max, string $ref, int $page): void {
    $results = $this->api->getCounterpartyContactPersons($ref, $page);
    if (!empty($results['data'])) {
      foreach ($results['data'] as $dataRow) {
        $resultData[$dataRow['Ref']] = $dataRow;
      }
      if ($max > count($resultData)) {
        $page++;
        $this->getContactPersonsAlters($resultData, $max, $ref, $page);
      }
    }
  }

  /**
   * Types of counterparties.
   */
  public function getTypesOfCounterparties($params = []) {
    $options = $this->getCache($params, 'getTypesOfCounterparties');
    if (!empty($options)) {
      return $options;
    }
    $options = [];
    $results = $this->api->getTypesOfCounterparties();
    if (!empty($results['data'])) {
      foreach ($results['data'] as $row) {
        $options[$row['Ref']] = $row['Description'];
      }
    }
    $this->setCache($options, $params, 'getTypesOfCounterparties');
    return $options;
  }

  /**
   * Forms of ownership.
   */
  public function getOwnershipFormsList($params = []) {
    $options = $this->getCache($params, 'getOwnershipFormsList');
    if (!empty($options)) {
      return $options;
    }
    $options = [];
    $results = $this->api->getOwnershipFormsList();
    if (!empty($results['data'])) {
      foreach ($results['data'] as $row) {
        $options[$row['Ref']] = $row['Description'];
      }
    }
    $this->setCache($options, $params, 'getOwnershipFormsList');
    return $options;
  }

  /**
   * Types of cargo.
   */
  public function getCargoTypes($params = []) {
    $options = $this->getCache($params, 'getCargoTypes');
    if (!empty($options)) {
      return $options;
    }
    $options = [];
    $results = $this->api->getCargoTypes();
    if (!empty($results['data'])) {
      foreach ($results['data'] as $row) {
        if ($row['Ref'] == 'TiresWheels') {
          continue;
        }
        if ($row['Ref'] == 'Pallet') {
          continue;
        }
        $options[$row['Ref']] = $row['Description'];
      }
    }
    $this->setCache($options, $params, 'getCargoTypes');
    return $options;
  }

  /**
   * Create a Contractor.
   */
  public function saveCounterparty($params) {
    return $this->api->saveCounterparty($params);
  }

  /**
   * Create Contractor Contact Person.
   */
  public function saveContactPerson($params) {
    return $this->api->saveContactPerson($params);
  }

  /**
   * Types of payers.
   */
  public function getTypesOfPayers($params) {
    $options = $this->getCache($params, 'getTypesOfPayers');
    if (!empty($options)) {
      return $options;
    }
    $options = [];
    $results = $this->api->getTypesOfPayers();
    if (!empty($results['data'])) {
      foreach ($results['data'] as $row) {
        if ($row['Ref'] == 'ThirdPerson') {
          continue;
        }
        $options[$row['Ref']] = $row['Description'];
      }
    }
    $this->setCache($options, $params, 'getTypesOfPayers');
    return $options;
  }

  /**
   * Forms of payment.
   */
  public function getPaymentForms($params) {
    $options = $this->getCache($params, 'getPaymentForms');
    if (!empty($options)) {
      return $options;
    }
    $options = [];
    $results = $this->api->getPaymentForms();
    if (!empty($results['data'])) {
      foreach ($results['data'] as $row) {
        $options[$row['Ref']] = $row['Description'];
      }
    }
    $this->setCache($options, $params, 'getPaymentForms');
    return $options;
  }

  /**
   * Create an express delivery note.
   */
  public function internetDocument($params) {
    return $this->api->saveInternetDocumentNew($params);
  }

  /**
   * Edit express waybill.
   */
  public function internetDocumentUpdate($params) {
    $update = $this->api->internetDocumentUpdate($params);
    if (!empty($update['data'][0]['Ref'])) {
      foreach ($update['data'][0] as $key => $value) {
        $params[$key] = $value;
      }
      $this->insertEn($params);
    }
    return $update;
  }

  /**
   * Updating the list of invoices.
   */
  public function updateDocumentList($params): void {

    // Clear old items:
    $this->db->truncate('novaposhta_en')
      ->execute();

    $results = $this->api->getDocumentList($params);
    if (!empty($results['data'])) {
      $docs = [];
      foreach ($results['data'] as $row) {
        $this->insertEn($row);
        $docs[] = $row['IntDocNumber'];
      }
      $this->getStatusDocuments($docs);
    }
  }

  /**
   * Recording of invoice data in the database.
   */
  public function insertEn($row): void {
    $address = array_filter([
      $row['CityRecipientDescription'] ?? NULL,
      $row['RecipientAddressDescription'] ?? NULL,
    ]);
    if (empty($address)) {
      $address = array_filter([
        $row['RecipientCityName'] ?? NULL,
        $row['RecipientAddressName'] ?? NULL,
        $row['RecipientHouse'] ?? NULL,
        $row['RecipientFlat'] ?? NULL,
      ]);
    }

    $this->db->merge('novaposhta_en')
      ->keys([
        'ref' => $row['Ref'],
      ])
      ->fields([
        'ref' => $row['Ref'],
        'order_id' => !empty($row['InfoRegClientBarcodes']) ? $row['InfoRegClientBarcodes'] : 0,
        'en_num' => $row['IntDocNumber'],
        'created' => strtotime($row['DateTime']),
        'delivery_day' => strtotime($row['EstimatedDeliveryDate']),
        'cost' => $row['Cost'],
        'cost_delivery' => $row['CostOnSite'],
        'weight' => $row['Weight'],
        'seats_amount' => $row['SeatsAmount'],
        'description' => $row['Description'],
        'address' => implode(',<br/>', $address),
        'status' => !empty($row['StateName']) ? $row['StateName'] : NULL,
        'date_receiving' => !empty($row['RecipientDateTime']) ? strtotime($row['RecipientDateTime']) : NULL,
        'all_info' => serialize($row),
      ])
      ->execute();
  }

  /**
   * Generates the print link for the specified order number based.
   *
   * @param string $enNum
   *   The order number for which the print link is to be generated.
   */
  public function getPrintLink($enNum) {
    $apiKey = $this->apiKEY;
    $link = "https://my.novaposhta.ua/orders/printDocument/orders/$enNum/type/pdf/apiKey/$apiKey";
    switch (\Drupal::config('novaposhta.en.settings')->get('config.print')) {
      case'printDocumentPdf':
        break;

      case'printMarking100x100':
        $link = "https://my.novaposhta.ua/orders/printMarking100x100/orders/$enNum/type/pdf/apiKey/$apiKey/zebra";
        break;

      case'printMarking85x85':
        $link = "https://my.novaposhta.ua/orders/printMarking85x85/orders/$enNum/type/pdf/apiKey/$apiKey/zebra";
        break;
    }
    return $link;
  }

  /**
   * Retrieves status information for a list of documents by their numbers.
   *
   * @param array $enNums
   *   An array of document numbers for which the status needs to be retrieved.
   * @param string|null $parentNum
   *   (Optional) The parent document number for forwarding-related information.
   */
  public function getStatusDocuments(array $enNums, string $parentNum = NULL) {
    if (empty($enNums)) {
      return NULL;
    }
    $params = [
      'Documents' => [],
    ];
    foreach ($enNums as $enNum) {
      $params['Documents'][] = [
        'DocumentNumber' => $enNum,
      ];
    }
    $results = $this->api->getTrackingDocument($params);

    if (!empty($results['data'])) {
      foreach ($results['data'] as $datum) {

        // Update info:
        $updateFields = array_filter([
          'status' => $datum['Status'] ?? NULL,
          'weight' => $datum['DocumentWeight'] ?? 0,
          'cost_delivery' => $datum['DocumentCost'] ?? 0,
        ]);

        if (!empty($datum['ScheduledDeliveryDate'])) {
          $updateFields['delivery_day'] = strtotime($datum['ScheduledDeliveryDate']);
        }

        if (!empty($datum['ScheduledDeliveryDate'])) {
          $updateFields['delivery_day'] = strtotime($datum['ScheduledDeliveryDate']);
        }

        if (!empty($datum['RecipientDateTime'])) {
          $updateFields['date_receiving'] = strtotime($datum['RecipientDateTime']);
        }

        if (!empty($datum['LastCreatedOnTheBasisDocumentType'])
          && $datum['LastCreatedOnTheBasisDocumentType'] == 'Redirecting'
          && !empty($datum['LastCreatedOnTheBasisNumber'])) {
          $updateFields['new_en_num'] = $datum['LastCreatedOnTheBasisNumber'];
        }

        $updateFields['address'] = implode('<br/>', array_filter([
          $datum['CityRecipient'] ?? NULL,
          $datum['WarehouseRecipient'] ?? NULL,
        ]));

        if (empty($updateFields['address']) && !empty($datum['WarehouseRecipientAddress'])) {
          $updateFields['address'] = $datum['WarehouseRecipientAddress'];
        }
        if (!empty($parentNum)) {

          // We save forwarding data:
          $parentData = $this->db->select('novaposhta_en', 'n')
            ->fields('n', ['all_info'])
            ->condition('n.en_num', $parentNum)
            ->execute()->fetchField();
          if (!empty($parentData)) {
            $parentData = unserialize($parentData);
            $parentData['RedirectingData'] = $datum;
            $updateFields['all_info'] = serialize($parentData);
          }

          $datum['Number'] = $parentNum;
        }

        if (!empty($datum['Number']) && !empty($updateFields)) {
          $this->db->update('novaposhta_en')
            ->fields($updateFields)
            ->condition('en_num', $datum['Number'])
            ->execute();
        }

        if (!empty($updateFields['new_en_num'])) {
          $this->db->update('novaposhta_en_orders')
            ->fields([
              'new_en_num' => $updateFields['new_en_num'],
            ])
            ->condition('en_num', $datum['Number'])
            ->execute();

          if (count($enNums) == 1) {
            return $this->getStatusDocuments([$datum['LastCreatedOnTheBasisNumber']], $datum['Number']);
          }
          else {
            $this->getStatusDocuments([$datum['LastCreatedOnTheBasisNumber']], $datum['Number']);
          }
        }
      }
      return count($enNums) == 1 ? reset($results['data']) : $results['data'];
    }
    return NULL;
  }

  /**
   * Deletes an internet document.
   *
   * @param array $params
   *   Parameters required for deleting the internet document.
   */
  public function internetDocumentDelete(array $params) {
    $this->api->internetDocumentDelete($params);
  }

  /**
   * Searches settlements based on the provided city name.
   *
   * @param string $search
   *   The name of the city to search for settlements.
   */
  public function searchSettlements(string $search) {
    return $this->api->searchSettlements([
      'CityName' => trim($search),
      'Limit' => 20,
    ]);
  }

  /**
   * Retrieves the reference code of a city by its name.
   *
   * @param string $search
   *   The name of the city to search for.
   */
  public function getCityRefByName(string $search) {
    $results = $this->api->searchSettlements([
      'CityName' => trim($search),
      'Limit' => 1,
    ]);
    return !empty($results['data'][0]['Addresses'][0]['Ref']) ? $results['data'][0]['Addresses'][0]['Ref'] : '';
  }

  /**
   * Searches for streets within a settlement based on the provided search term.
   *
   * @param string $search
   *   The search term to locate specific streets.
   * @param string $ref
   *   The reference identifier for the settlement.
   */
  public function searchSettlementStreets(string $search, string $ref) {
    return $this->api->searchSettlementStreets([
      'StreetName' => trim($search),
      'SettlementRef' => $ref,
      'Limit' => 20,
    ]);
  }

  /**
   * Retrieves the message code text.
   */
  public function getMessageCodeText() {
    $results = $this->getCache([], 'getMessageCodeText');
    if (!empty($results)) {
      return $results;
    }
    $response = $this->api->getMessageCodeText();
    $results = !empty($response['data']) ? $response['data'] : [];
    $this->setCache($results, [], 'getMessageCodeText');
    return $results;
  }

  /**
   * Get cache data.
   */
  public function getCache($params, $type) {
    if ($this->notCache) {
      return NULL;
    }
    if ($this->apiKEY !== $this->apiKEYDef) {
      return NULL;
    }
    $options = $this->novaPoshta->variableGet($this->getFileName($params, $type), $this->dir);
    return !empty($options) ? $options : [];
  }

  /**
   * Save cache data.
   */
  public function setCache($options, $params, $type) {
    if ($this->notCache) {
      return NULL;
    }
    if ($this->apiKEY !== $this->apiKEYDef) {
      return NULL;
    }
    $this->novaPoshta->variableSet($options, $this->getFileName($params, $type), $this->dir);
  }

  /**
   * Generates a file name based on the given parameters and type.
   */
  public function getFileName($params, $type) {
    if (!is_array($params)) {
      \Drupal::logger('params')->notice('<pre>' . print_r($params, TRUE) . '</pre>');
    }
    return $type . '-' . implode('_', $params);
  }

}
